# 4DCT(MRI)

**Code for creating synthetic 4DCT(MRI)s from a reference CT and a 4DMRI**

This is the git repository for the updated 4DCT(MRI) phantom workflow including ribcage motion and lung density scaling as described by T. Jenny et al. (2022) [1].

The previous version of the workflow as described by A. Duetschler et al. (2022) [2] can be found [here](https://gitlab.psi.ch/duetschler_a/4DCT-MRI/). 

----------------------------------------------------------------------------------------------------

Please cite the following publications when using the code:

Jenny, T., Duetschler, A., Weber, D.C., Lomax, A.J. and Zhang, Y. (2023), *Technical Note: Towards more realistic 4DCT(MRI) numerical lung phantoms*. Med. Phys.. [https://doi.org/10.1002/mp.16451](https://doi.org/10.1002/mp.16451)

Duetschler, A., Bauman, G., Bieri, O., Cattin, P.C., Ehrbar, S., Engin-Deniz, G., Giger, A., Josipovic, M., Jud, C., Krieger, M., Nguyen, D., Persson, G.F., Salomir, R., Weber, D.C., Lomax, A.J. and Zhang, Y. (2022), *Synthetic 4DCT(MRI) lung phantom generation for 4D radiotherapy and image guidance investigations*. Med. Phys.. [https://doi.org/10.1002/mp.15591](https://doi.org/10.1002/mp.15591)

Duetschler, A., Jenny, T., Weber, D.C., Lomax, A.J. and Zhang, Y. (2022). Updated 4DCT(MRI). Zenodo. [doi:10.5281/zenodo.7052585](https://doi.org/10.5281/zenodo.7052585)

In case of questions, please contact Alisha Dütschler (alisha.duetschler@psi.ch).

----------------------------------------------------------------------------------------------------

**4DMRI lung and ribcage mesh data**

Additional 4DMRI data in the form of moving lung and ribcage meshes suitable for the generation of 4DCT(MRI)s can be found [here](https://gitlab.psi.ch/4DCT-MRI_updated/4DMRI_moving_meshes/).

![Workflow](Figures/4D-CT(MRI)workflow.jpg)

***Figure 1.*** Workflow for 4DCT(MRI) generation.

## Setup instructions and prequisites

Most of the code is written in MATLAB. There are, however, also parts written in java and c++. In this case we provide the exectuables for direct use as well as the source code.

**1. Clone repository**

You can clone the repository from Gitlab. In the terminal navigate to the desired location and clone the repository

    git clone git@gitlab.psi.ch:4DCT-MRI_updated/4DCT-MRI.git

**2. Download plastimatch**

The open-source software [Plastimatch](http://plastimatch.org/) is used for image registration. Please follow the download instructions on the website. The use of other image registration softwares is also possible.
Plastimatch can also be used to convert dicom data to mha data and back.

**3. Other usefull tools**

You might also consider to download [Slicer](https://www.slicer.org/) (for image segmentation and landmark definition), the [Scalismo Ui](https://github.com/unibas-gravis/scalismo-ui/) (for landmark definition), [ParaView](https://www.paraview.org/) (for mesh visualization) and [vv](https://www.creatis.insa-lyon.fr/rio/vv/) (for mha visualization).

We provide exectables for the java code, however the source code is also provided and you would require a java setup (e.g. in eclipse) and vtk libraries for further developements of these codes.


----------------------------------------------------------------------------------------------------

## Documentation

----------------------------------------------------------------------------------------------------
We suggest you follow the naming conventions for the necessary files suggested below, otherwise further changes to the code at different levels might be needed.

**Prequisites**

- reference CT in mha format (called CT.mha) in a seperate folder (referred to as CTfolder from now on)
- 4DMRI (or 4DCT)
    - reference MRI state in mha format (called MRI.mha) in a seperate folder (referred to as MRIfolder from now on)
    - moving MRI meshes (MRIfolder/MovingMRmesh) for both lung halves and ribcage for the different states
    - or deformation vector fields of each each 4DMRI state registered to the reference MRI state (external_state_000X.mha and internal_state_000X.mha etc in folder MRIfolder/dvfs)
      two Vector fields are used to model the internal (lungs, hearts, etc.) and external region (ribcage, muscles, etc.) independently to preserve sliding organ motion
    
Notes: 
- The same reference breathing state should be chosen for the CT and MRI (i.e. end exhale for both or DIBH CT and an end inhalation MRI reference state).
- Make sure the CT and 4DMRI have the same orientation.
- The 4DMRI registration can for example be performed using the open-source [Plastimatch](http://plastimatch.org/) software.

**O. segmentation**

*Note: The segmentation of the MRI structures is only needed if the MR meshes need to be created.
* Procedure:
* [ ] The first step is the segmentation of both lung halves (separately). This can for example be done using the open-source software software [Slicer](https://www.slicer.org/) or [itkSnap](http://www.itksnap.org/). The segmentation results should be saved in the folders CTfolder/structures and MRIfolder/structures as lungs_left.mha and lungs_right.mha. The value 255 has to be asigned to the lung halves and 0 otherwise.

* [ ] A mask has to be generate of the ribcage (see pink area in left part of Figure 2), saved in the folders CTfolder/structures and MRIfolder/structures as ribcage.mha.
      The value 255 has to be asigned to the non-moving parts and 0 otherwise.

* [ ] Additionally, a mask of the body is needed, saved in CTfolder/structures as body.mha.

* [ ] Based on the ribcage and the body mask a mask of the internal region (see pink area in right part of Figure 2) can be generated using the **createInternalRegionMask.m** in the
      4DCT-MRI/code/0_segmentation folder. The result should be saved under CTfolder/structures/internal_region.mha. (And also for the MRI if required for motion extraction using deformable image registration.) Alternatively, it is also possible to manually segement the internal region and use it for an automatic generation of the external ribcage region.

<p align="middle">
  <img src="Figures/ribcage.png" width="48%" />
  <img src="Figures/internalRegion.png" width="48%" /> 
</p>

***Figure 2.*** Visualization of external ribcage region (left) and internal region (right) in pink.

* Output:
    * lungs_left.mha, lungs_right.mha, body.mha, ribcage.mha and internal_region.mha in the folder CTfolder/structures
    * lungs_left.mha, lungs_right.mha and ribcage.mha in the folder MRIfolder/structures

**1. Reference mesh generation**

The next step is the generation of lung and ribcage meshes for the CT and the reference MRI. This is done using vtk in java. The java source code can be found in 4DCT-MRI/code/1_meshGeneration together with the exectuables.

If you use the provided volunteer 4DMRI motion data, the meshes are already provided and you can skip the genration of the MRI mesh.

* TOOL: **BinaryImageToMesh**

* Procedure/input:
* [ ] navigate to the 1_meshGeneration/BinaryImageToMesh folder
* [ ] ./BinaryImageToMesh CTfolder/structures/lungs_left.mha CTfolder/structures/lungs_left.vtk 
* [ ] ./BinaryImageToMesh CTfolder/structures/lungs_right.mha CTfolder/structures/lungs_right.vtk
* [ ] ./BinaryImageToMesh CTfolder/structures/ribcage.mha CTfolder/structures/ribcage.vtk
* repeat for  MRIfolder if you want to use your own motion data

* Output:
    * meshes (lungs_left.vtk, lungs_right.vtk, ribcage.vtk) and smoothed masks (lungs_left_smooth.mha, lungs_right_smooth.mha, ribcage_smooth.mha) in the folder CTfolder/structures
    * meshes (lungs_left.vtk, lungs_right.vtk, ribcage.vtk) and smoothed masks (lungs_left_smooth.mha, lungs_right_smooth.mha, ribcage_smooth.mha) in the folder MRIfolder/structures)

Technically, only the meshes for the reference MRI are used and not for the CT, however, the CT meshes can be used to compare the result of the registration and the warping of the MRI mesh. 

Additionally the code also saves a lungs_left_smooth.mha, lungs_right_smooth.mha and ribcage_smooth.mha for the CT and MRI. This is obtained from the original mask through a downsamling to 5x5x5 mm³ and a Gaussian smoothing and will be used in the next step (alternatively you can also use the SmoothBinaryImage function for this). The smoothing and resampling is not necessary but usually lead to better registration results. 

If you prefer to use another method to generate triangle meshes, please make sure your meshes don't have too many mesh points as this could lead to memory issues or long computation times.

There is also a script to generate binary mha files from vtk surface meshes (MeshToBinaryImage) which can be useful.

**2. Establish mesh correspondence through the registration of binary masks**

Correspondence is established through a plastimatch registration of the smoothed masks of the reference MRI and the CT for both lung halves and the ribcage separately. The results of the registration are used to warp the MRI surface mesh into a corresponding mesh for the CT.

These steps are all performed in the callDIRmeshcorrespondence.m script. After setting up the appropriate paths, the function DIRmeshcorrespondence is called, which adapts the plastimatch template command file (plastimatch_align_center_affine_bspline_template.txt) and performs the registration and then uses the deformation vector field to warp the CT mesh. The registration process is done separately for each lung half and the ribcage.

* TOOL: **callDIRmeshcorrespondence.m**

* Procedure:
* [ ] Adapt CT and MRI numbers and paths. 
* [ ] Define ribcage landmarks¹. Typically use the sternum and two points on the spine (at extent of lungs). Further landmarks can be chosen at the inferior extent of either the CT or the MRI (see Figure 3). Save the landmarks as lms_ribcage_MRI.csv and lms_ribcage_CT.csv in CTMRI_path.
* [ ] Optional: rerun with (more) landmarks by setting useLandmarks = true and entering the path to both landmark files¹.

* OUTPUT: for each side (left/right) and the ribcage    
    * plastimatch_side.txt, plastimatch_ribcage.txt command file
    * warped_mask_side.mha/warped_mask_ribcage.mha: result of registration (should be similar to MRI mask)
    * vf_side.mha/vf_ribcage.mha: deformation vector field from registration
    * moving_lungs_side.vtk/moving_ribcage.vtk and fixed_lungs_side.vtk/fixed_ribcage.vtk: copy of CT and MRI lung/ribcage mesh
    * deformed_lungs_side.vtk/defromed_ribcage.vtk: new corresponding CT lungs/ribcage obtained by applying the vf_side.mha/vf_ribcage.mha to points of fixed_lungs_side.vtk/fixed_ribcage.vtk
    
Suggested workflow: Try a registration (for both sides) without landmarks first and deform the CT mesh by executing the callDIRmeshcorrespondence.m script. Load the resulting meshes deformed_lungs_left.vtk and deformed_lungs_right.vtk into ParaView and compare them to the original moving_lungs_left.vtk and moving_lungs_right.vtk of the reference CT. The correspondence can also be checked in paraview by comparison to fixed_lungs_left.vtk and fixed_lungs_right.vtk of the reference MRI.
If parts are missing or are too large, you can define one or more landmarks in these locations and redo the previous steps using the landmarks to guide the registration.  
Similarly for the ribcage.vtk meshs but define some landmarks from the beginning (see above)

Note:
¹ **Landmarks** can be defined by loading meshes in the [Scalismo Ui](https://github.com/unibas-gravis/scalismo-ui/) and clicking landmarks either on slices or on 3D meshes in the same location for the CT and the MRI. After saving the landmarks in a csv file, the sign of the first two coordinates has to be flipped ((x,y,z)-->(-x,-y,z)).
Alternatively, [Slicer](https://www.slicer.org/) can also be used to define landmarks, but only on slices not on the 3D mesh. Also save the landmarks in a csv file. Slicer allready uses the correct coordinate system for Plastimatch. 
Make sure to use the same names in both landmark files for corresponding landmarks and use the same order.

<p align="middle">
  <img src="Figures/landmarkposition1.png" width="48%" />
  <img src="Figures/landmarkposition2.png" width="48%" /> 
</p>

***Figure 3.*** Suggested landmark positions used to guide the DIR of the ribcage are indicated in pink as an example on a coronal (a) and sagittal slice.

In case directly calling Plastimatch from the matlab script fails, the callPreparePlastimatchDIRfiles.m can be used followed by a plastimatch registration in the terminal. Subsequently, the callWarpSurfaceMesh.m script can be called.

**3. Insert points inside meshes and apply motion to CT meshes**

If you want to use your own motion data you first have to create moving meshes. Each state of the 4DMRI has to be registered to the reference MRI state and the resulting deformation vector fields are used to warp the reference meshes. The internal and external region are registered independently and used to warp lung meshes and the ribcage mesh in order to preserve sliding organ motion. You can use the createMovingMRmeshes.m script for this. If you use the provided 4DMRI motion data the moving meshes are already provided and you can skip this step.

The following script combines all the remaining steps needed in order to generate a new 4DCT(MRI). The steps involved are described below. 
* TOOL: **createMovingCTmeshes.m**

* Procedure:
* [ ] Adapt CT and or MRI numbers and paths. 
* [ ] Adapt the start and end phase for the 4DMRI. 
* [ ] Run MATLAB script.

* Steps:
    * insertGridPointsInsideCTmesh.m: for ribcage and both sides insert corresponding points also inside the CT meshes. This relies on a open-source MATLAB triangle/ray intersection implementation [3].
        * Output: lungs_left_grid.vkt, lungs_right_grid.vtk, ribcage_grid.vtk, lungs_left_surface_and_grid.vtk, lungs_right_surface_and_grid.vtk and ribcage_surface_and_grid.vtk in folder MovingCTmesh.
    * applyMotionToCTmesh.m: applies motion of MR mesh points to corresponding CT mesh points resulting in a CT mesh for each state of the 4DMRI.
        * Output: folder MovingCTmesh/Side and MovingCTmesh/ribcage with a state_000X.mha mesh for each state X.
         
**4. Combine and correct motion and warp reference CT (including lung density scaling)**

The following script combines all the remaining steps needed in order to generate a new 4DCT(MRI). The steps involved are described below. 
* TOOL: **createVectorFieldsAndWarp.m**

* Procedure:
* [ ] Adapt CT and or MRI numbers and paths. 
* [ ] Adapt the reference CT phase and the start and end phase for the 4DMRI. 
* [ ] Run MATLAB script.

* Steps:
    * prepareSeparateVFs: executable from C++ code. Creates the distance maps needed for the implementation of the sliding organ motion and separate interpolated vectorfields for ribcage and internal region based on CT mesh motion.
        * Output: folder DistanceMaps with lung_distanceMap_000X.mha, ribcage_distanceMap_000X.mha, added_distanceMap_000X.mha, multiply_distanceMap_000X.mha and OriginalGradient_X.mha
                  Separate vectorfields for ribcage and internal region (MotionVectors/ribcage/motionfield_000X.mha, MotionVectors/ribcage/deformationfield_000X.mha, MotionVectors/lungs/motionfield_000X.mha and MotionVectors/lungs/deformationfield_000X.mha).
    * A diffusor with a smooth fall-off from the sliding chest wall interface is created.
        * Output: diffusor_steps100_000X.mha
    * combineVFsAndWarp: executable from C++ code. Combines and corrects motion and deformation vector field and warps reference CT. Includes lung density scaling.
        * Output: mha files of the states of the 4DCT-MRI in the folder MovingCT and motionfields used to warp the reference CT and inverse deformationfields in folder /MotionVectors. 

----------------------------------------------------------------------------------------------------

## Example data

----------------------------------------------------------------------------------------------------

The provided example CT was modified from the The Cancer Image Archive [4] 4D-Lung dataset [5-8] licensed under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/). The CT is from patient 115 (50% phase, end exhale), which was converted from dicom to mha format. Each lung half and the ribcage was manually contoured on the reference phase.

The provided 4DMRI lung mesh data are from MRI1 and more data can be found [here](https://gitlab.psi.ch/4DCT-MRI_updated/4DMRI_moving_meshes/).

Due to data storage constraints on git, only a single breathing cycle 4DCT(MRI) is available here. Further, most intermediate results had to be removed and only a limited selection is stored to demonstrate the folder structure.

----------------------------------------------------------------------------------------------------

## References

----------------------------------------------------------------------------------------------------


[1] Jenny, T., Duetschler, A., Weber, D.C., Lomax, A.J. and Zhang, Y. (2023), Technical note: Towards more realistic 4DCT(MRI) numerical lung phantoms. 1-12. DOI: 10.1002/mp.16451

[2] Duetschler, A., Bauman, G., Bieri, O., Cattin, P. C., Ehrbar, S., Engin-Deniz, G., Giger, A., Josipovic, M., Jud, C., Krieger, M., Nguyen, D., Persson, G. F., Salomir, R., Weber, D. C., Lomax, A. J. and Zhang, Y. (2022). Synthetic 4DCT(MRI) lung phantom generation for 4D radiotherapy and image guidance investigations. Medical physics, 49(5), 2890-2903. DOI: 10.1002/mp.1559

[3] Tuszynski, J. (2021). Triangle/Ray Intersection (https://www.mathworks.com/matlabcentral/fileexchange/33073-triangle-ray-intersection), MATLAB Central File Exchange. Retrieved June 11, 2021. 

[4] Clark, K., Vendt, B., Smith, K., Freymann, J., Kirby, J., Koppel, P., Moore, S., Phillips, S., Maffitt, D., Pringle, M., Tarbox L. and Prior, F. (2013). The Cancer Imaging Archive (TCIA): maintaining and operating a public information repository. Journal of Digital Imaging. 2013 Dec;26(6):1045-57. DOI: 10.1007/s10278-013-9622-7 

[5] Hugo, G. D., Weiss, E., Sleeman, W. C., Balik, S., Keall, P. J., Lu, J. and Williamson, J. F. (2016). Data from 4D Lung Imaging of NSCLC Patients. The Cancer Imaging Archive. http://doi.org/10.7937/K9/TCIA.2016.ELN8YGLE

[6] Hugo, G. D., Weiss, E., Sleeman, W. C., Balik, S., Keall, P. J., Lu, J. and Williamson, J. F. (2017). A longitudinal four-dimensional computed tomography and cone beam computed tomography dataset for image-guided radiation therapy research in lung cancer. Med. Phys., 44: 762–771. doi:10.1002/mp.12059

[7] Balik, S., Weiss, E., Jan, N., Roman, N., Sleeman, W. C., Fatyga, M., Christensen, G. E., Zhang, C., Murphy, M. J., Lu, j., Keall P. J., Williamson, J. F. and Hugo, G. D. (2013). Evaluation of 4-Dimensional Computed Tomography to 4-Dimensional Cone-Beam Computed Tomography Deformable Image Registration for Lung Cancer Adaptive Radiation Therapy. Int. J. Radiat. Oncol. Biol. Phys. 86, 372–9. PMCID: PMC3647023.

[8] Roman, N. O., Shepherd, W., Mukhopadhyay, N., Hugo, G. D. and Weiss, E. (2012). Interfractional Positional Variability of Fiducial Markers and Primary Tumors in Locally Advanced Non-Small-Cell Lung Cancer during Audiovisual Biofeedback Radiotherapy. Int. J. Radiat. Oncol. Biol. Phys. 83, 1566–72. DOI:10.1016/j.ijrobp.2011.10.051

