clear all; clc; close all;
addpath('../matlab-scripts');
addpath('functions');

%Script for 4DCT(MRI) generation
%Created by Alisha Duetschler August 2019
%Adapted in June 2021
%Adapted by Timothy Jenny in August 2022 to further include ribcage motion

%Prequisites: 
%   -Moving CT meshes
%   -Lung masks (lungs_left.mha and lungs_right.mha combined into lungs.mha) with zero values outside of the lungs
%   -internal mask (internal_region.mha) with zero values outside of the internal region
%   -Body mask (body.mha)  with zero values outside of the body
%   -Reference CT (CT.mha)
%   -Moving CT lung and ribcage meshes

%   1) prepareSeparateVFs: create ribcage and lung VFs through interpolation of meshes. Create distance maps.
%   2) diffusion from sliding organ interface.
%   3) combineVFsAndWarp: combine and correct VFs, lung density scaling and warping of reference CT.

%% 

%% input that has to be changed
CTNo = 1;
MRINo = 1;

CTbasepath = '../../example/data/CT/CT';
MRIbasepath = '../../example/data/MRI/MRI';
CT_MR_basepath = '../../example/results/';

reference_phase = 23; %here:end exhale
start_phase = 23; %one cycle
end_phase = 32;

diffusion_Steps = 100;  %We used 100 in examples, but probably 50 or maybe even less should also work.

N_Cores = 1;  %Simple Parallelization Scheme. Attention it needs a lot of memory

%% Prepare Some BasicParallelism
distribution = getParallelismDistribution(N_Cores, start_phase, end_phase);
dist_size = size(distribution);
N_Cores = dist_size(2);
disp(N_Cores)

%% define paths
CT_path = [CTbasepath num2str(CTNo) '/']
MR_path = [MRIbasepath num2str(MRINo) '/']
CT=[CT_path 'CT.mha'];

CT_MR_path = [CT_MR_basepath 'MRI' num2str(MRINo) '_CT' num2str(CTNo) '/'];

lung_mask=[CT_path 'structures/lungs.mha'];
internal_mask= [CT_path 'structures/internal_region.mha'];
body_mask = [CT_path 'structures/body.mha'];

%% Create additional masks
%  %% Create internal region masks
% ribcage_mask=[CT_path 'structures/ribcage.mha'];
% [ribcage header]=read_MHA(ribcage_mask); 
% internal = 255* ((body - ribcage)>0);
% writemha(internal_mask , internal , header.origin , header.spacing,'uchar');

%% Create the extended Bodymask (needed to cutoff the deformation field at some point to not deform the couch)
body_mask_extended_path = [CT_path 'structures/body_extended.mha'];
[body header]=read_MHA(body_mask); 
body_extended = imdilate(body, strel('sphere',5));
writemha(body_mask_extended_path , body_extended , header.origin , header.spacing,'uchar');

%% set up directories
cmd = ['mkdir -p ' CT_MR_path];
system(cmd);

cmd=['mkdir -p ' CT_MR_path 'MotionVectors/'];
system(cmd);

cmd=['mkdir -p ' CT_MR_path 'MotionVectors/ribcage/'];
system(cmd);

cmd=['mkdir -p ' CT_MR_path 'MotionVectors/lungs/'];
system(cmd);

cmd=['mkdir -p ' CT_MR_path 'MovingCT/'];
system(cmd);

cmd=['mkdir -p ' CT_MR_path 'DistanceMaps/'];
system(cmd);

%read dimensions of CT from header (you might have to decrease the min
%value slightly or increase the max value slightly if an error occurs. This
%could be, if the lung/ribcage is very close to the edge of the CT and the
%defromed lung/ribcage mesh extends outside for some states). A margin of 
% 100 everywhere is used to make sure it works well. However
%one can also try out smaller margins.
CT_header = mha_read_header(CT);
xmin=-100;
xmax=CT_header.Dimensions(1)-1+100;
ymin=-100;
ymax=CT_header.Dimensions(2)-1+100;
% zmin=0;
zmax=CT_header.Dimensions(3)-1+100;
zmin=-100;
disp('Warp reference CT to different phases');

%Command for preparing separate VFs (internal/external) and calcualtion of
%distance map
command_prepare = ['ccode/build/prepareSeparateVFs ' CT_MR_path ' ' num2str(reference_phase) ' ' num2str(distribution(1,1)) ' ' num2str(distribution(2,1)) ' ' CT ' ' internal_mask ' ' num2str(xmin) ' ' num2str(xmax) ' ' num2str(ymin) ' ' num2str(ymax) ' ' num2str(zmin) ' ' num2str(zmax)];
for i = 2:N_Cores
    command_cores = ['ccode/build/prepareSeparateVFs ' CT_MR_path ' ' num2str(reference_phase) ' ' num2str(distribution(1,i)) ' ' num2str(distribution(2,i)) ' ' CT ' ' internal_mask ' ' num2str(xmin) ' ' num2str(xmax) ' ' num2str(ymin) ' ' num2str(ymax) ' ' num2str(zmin) ' ' num2str(zmax)];
    command_prepare = [command_prepare ' & ' command_cores];
end

ccFile = [CT_MR_path '/Run_prepareSeparateVFs.txt'];       
fileID = fopen(ccFile, 'w');
fprintf(fileID,'module load gcc/7.4.0\n');
fprintf(fileID,command_prepare);
fclose(fileID);

system(['chmod +x ' ccFile]);
system(ccFile);

%% Create the Diffusor
for phase=start_phase:end_phase    
    multMap_Name = [CT_MR_path 'DistanceMaps/multiplied_distanceMap_' num2str(phase,'%04d') '.mha']
    diffusion_SaveName = [CT_MR_path 'DistanceMaps/diffusor_steps' num2str(diffusion_Steps) '_' num2str(phase,'%04d') '.mha']
    getAndSaveDiffusor(multMap_Name, diffusion_SaveName, diffusion_Steps);
end

%% execute combineVFsAndWarp with the diffusion Map from Above
%%Command for Running
command_run = ['ccode/build/combineVFsAndWarp ' CT_MR_path ' ' num2str(reference_phase) ' ' num2str(distribution(1,1)) ' ' num2str(distribution(2,1)) ' ' CT ' ' internal_mask ' ' lung_mask ' ' body_mask_extended_path ' ' num2str(diffusion_Steps)];
for i = 2:N_Cores
    command_cores = ['ccode/build/combineVFsAndWarp ' CT_MR_path ' ' num2str(reference_phase) ' ' num2str(distribution(1,i)) ' ' num2str(distribution(2,i)) ' ' CT ' ' internal_mask ' ' lung_mask ' ' body_mask_extended_path ' ' num2str(diffusion_Steps)];
    command_run = [command_run ' & ' command_cores];
end

ccFile = [CT_MR_path '/Run_combineVFsAndWarp.txt'];        
fileID = fopen(ccFile, 'w');
fprintf(fileID,'module load gcc/7.4.0\n');
fprintf(fileID,command_run);
fclose(fileID);

system(['chmod +x ' ccFile]);
system(ccFile);

disp('---------------------------------------------------');