#include <vtkSmartPointer.h>
#include <vtkPolyDataWriter.h>
#include <vtkPolyDataReader.h>
#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkDelaunay3D.h>
#include <vtkUnstructuredGrid.h>
#include <vtkTetra.h>

#include <itkImage.h>
#include <itkVector.h>
#include <itkImageFileReader.h>
#include <itkImageFileWriter.h>
#include <itkLinearInterpolateImageFunction.h>
#include <itkNearestNeighborInterpolateImageFunction.h>
#include <itkWarpImageFilter.h>
#include <itkImageDuplicator.h> 

#include <cstdlib>
#include <cstdio>

#include <itkPointSet.h>
#include <itkBSplineScatteredDataPointSetToImageFilter.h>

#include "itkCastImageFilter.h"
#include "itkSignedDanielssonDistanceMapImageFilter.h"
#include "itkGradientImageFilter.h"
#include <itkAddImageFilter.h>
#include <itkSubtractImageFilter.h>
#include <itkMultiplyImageFilter.h>

typedef itk::Vector<double, 3> VectorType;
typedef itk::Image<VectorType, 3> VectorImageType;
typedef itk::Image<short, 3> ImageType;
typedef itk::Image<unsigned char, 3> ImageMaskType;
typedef itk::ImageFileReader<ImageType> ImageReaderType;
typedef itk::ImageFileWriter<VectorImageType> VectorImageWriterType;
typedef itk::ImageFileReader<ImageMaskType> ImageMaskReaderType;

typedef itk::PointSet <VectorType, 3> PointSetType;
typedef itk::BSplineScatteredDataPointSetToImageFilter < PointSetType , VectorImageType > FilterType ;

// needed for no rounding errors when warping the image
typedef itk::Image<float, 3> FloatImageType;
typedef itk::ImageFileReader<FloatImageType> FloatImageReaderType;
typedef itk::ImageFileWriter<FloatImageType> FloatImageWriterType;
typedef itk::WarpImageFilter<FloatImageType, FloatImageType, VectorImageType >  FloatWarperType;
typedef itk::LinearInterpolateImageFunction<FloatImageType, double >  FloatInterpolatorType;

typedef itk::SignedDanielssonDistanceMapImageFilter<ImageMaskType, FloatImageType> DistanceMap;

typedef itk::GradientImageFilter<FloatImageType, float> GradientFilterType;

typedef itk::AddImageFilter<FloatImageType, FloatImageType> AddImageFilter;
typedef itk::SubtractImageFilter<FloatImageType, FloatImageType> SubtractImageFilter;

using MultiplyFilter = itk::MultiplyImageFilter<FloatImageType, FloatImageType, FloatImageType>;

using namespace std;

VectorImageType::PixelType ConstrainVectorToRegion(VectorImageType::RegionType ImageRegion, VectorImageType::PixelType vector, VectorImageType::IndexType outputindex, VectorImageType::SpacingType spacing)
//A vector is shortened/pulled back, if it points outside of the image region
{	
	VectorImageType::IndexType pointing_to_index;
	for (int j=0;j<3;++j)
	{
	  pointing_to_index[j]=outputindex[j]+static_cast<int>(round(vector[j]/spacing[j]));
	}
	
	if (ImageRegion.IsInside(pointing_to_index))
        {
	  return vector;
	}
	else 
	{
	  double fractionOfVector=0.95;
	  while (fractionOfVector>0)
	  {
	    VectorImageType::PixelType newvector;
	    for (int j=0;j<3;++j)
	    {
	      newvector[j]=vector[j]*fractionOfVector;
	      pointing_to_index[j]=outputindex[j]+static_cast<int>(round(newvector[j]/spacing[j]));
	    }
	    if (ImageRegion.IsInside(pointing_to_index))
	    {
	      // the vector should not point to close to the outside region area, so pull it a voxel back
	      double factOfVector_for_halfVoxel=sqrt(spacing[0]*spacing[0]+spacing[1]*spacing[1]+spacing[2]*spacing[2])/sqrt(vector[0]*vector[0]+vector[1]*vector[1]+vector[2]*vector[2]);
	      if (factOfVector_for_halfVoxel<1)
	      {
		for (int j=0;j<3;++j)
		{
		  newvector[j]=(1-factOfVector_for_halfVoxel)*newvector[j];
		}
		return newvector;
	      }
	      break;
	    }
	    fractionOfVector-=0.05;
	  }
	
	VectorImageType::PixelType nullvector;
	for (int j=0;j<3;++j)
	{
	  nullvector[j]=vector[j]*0;
	}
	return nullvector;
	}
}

PointSetType :: Pointer CreateRibcagePointSet(int refstep, int step, int deformationOrMotionfield, string stepsvtkprefix, double *roibounds)
//The vtk ribcage meshes for both, the reference step and the current step are read in. The displacement vector is caclulated for all mesh points and saved as a itk PointSet.
// deformationOrMotionfield==1 -> MotionVectorField
// deformationOrMotionfield==0 -> DeformationVectorField
{
	string movingvtkFilename;
	string fixedvtkFilename;

	if (deformationOrMotionfield==1)
	  {
	    char buffer[255];
	    sprintf(buffer,"%sMovingCTmesh/Ribcage/state_%04d.vtk",stepsvtkprefix.c_str() ,refstep);
	    movingvtkFilename="";
	    movingvtkFilename.append(buffer);
        
	    char buffer2[255];
	    sprintf(buffer2,"%sMovingCTmesh/Ribcage/state_%04d.vtk",stepsvtkprefix.c_str(),step);
	    fixedvtkFilename="";
	    fixedvtkFilename.append(buffer2);

	  }
	  else
	  {
        char buffer[255];
	    sprintf(buffer,"%sMovingCTmesh/Ribcage/state_%04d.vtk",stepsvtkprefix.c_str() ,step);
	    movingvtkFilename="";
	    movingvtkFilename.append(buffer);
        
	    char buffer2[255];
	    sprintf(buffer2,"%sMovingCTmesh/Ribcage/state_%04d.vtk",stepsvtkprefix.c_str(),refstep);
	    fixedvtkFilename="";
	    fixedvtkFilename.append(buffer2);
	  }
	  
	
    printf("Fixed    %s\n",fixedvtkFilename.c_str());
    printf("Moving   %s\n",movingvtkFilename.c_str());
        
    vtkSmartPointer<vtkPolyDataReader> fixedReader=vtkSmartPointer<vtkPolyDataReader>::New();
    fixedReader->SetFileName(fixedvtkFilename.c_str());
    fixedReader->Update();

    vtkSmartPointer<vtkPolyDataReader> movingReader=vtkSmartPointer<vtkPolyDataReader>::New();
    movingReader->SetFileName(movingvtkFilename.c_str());
    movingReader->Update();
    
	if (fixedReader->GetOutput()->GetNumberOfPoints() != movingReader->GetOutput()->GetNumberOfPoints())
	{
	  cout << "Ribcage: Fixed and moving points must be the same number of points!" << endl;
	  exit(1);
	}
	
	long int pointid=0;
	PointSetType :: Pointer pointSet = PointSetType :: New ();
	PointSetType :: PointType point;
	VectorType V;

	cout << "Creating set of known motion vectors..." << endl;

	// create bounding box zero motion points
	int dx=20;
	int *roiedge=new int[3];

	for (roiedge[0]=0;roiedge[0]<=dx;++roiedge[0])
	{
	  for (roiedge[1]=0;roiedge[1]<=dx;++roiedge[1])
	  {
	    for (roiedge[2]=0;roiedge[2]<=dx;++roiedge[2])
	    {
	      if (roiedge[0]==0 || roiedge[0]==dx || roiedge[1]==0 || roiedge[1]==dx || roiedge[2]==0 || roiedge[2]==dx) // do create zero motion vectors only on the surface of the roi box
	      {
		for (int i=0;i<3;++i)
		{
		  point[i]=roibounds[2*i]+roiedge[i]*(roibounds[2*i+1]-roibounds[2*i])/dx;
		}
		V[0]=0;
		V[1]=0;
		V[2]=0;
		pointSet -> SetPoint ( pointid , point );
		pointSet -> SetPointData ( pointid , V );
		++pointid;
	      }
	    }
	  }
	}

	for (int i=0;i<fixedReader->GetOutput()->GetNumberOfPoints();++i)
	{
	  double *fixpoint=fixedReader->GetOutput()->GetPoint(i);
	  double *movpoint=movingReader->GetOutput()->GetPoint(i);

	  V[0]=movpoint[0]-fixpoint[0];
	  V[1]=movpoint[1]-fixpoint[1];
	  V[2]=movpoint[2]-fixpoint[2];
	  point[0]=fixpoint[0];
	  point[1]=fixpoint[1];
	  point[2]=fixpoint[2];

	  pointSet -> SetPoint ( pointid , point );
	  pointSet -> SetPointData ( pointid , V );
	  ++pointid;
	}

    return pointSet;
}

PointSetType :: Pointer CreateLungPointSet(int refstep, int step, int deformationOrMotionfield, string stepsvtkprefix, double *roibounds)
//The vtk lung meshes for both, the reference step and the current step are read in for the left and right side separately. The displacement vector is caclulated for all mesh points (both sides) and saved as a itk PointSet.
//combining the the meshes.
// deformationOrMotionfield==1 -> MotionVectorField
// deformationOrMotionfield==0 -> DeformationVectorField
{
	string movingvtkFilenameL;
	string fixedvtkFilenameL;
	string movingvtkFilenameR;
	string fixedvtkFilenameR;

	if (deformationOrMotionfield==1)
	  {
	    char bufferL[255];
	    sprintf(bufferL,"%sMovingCTmesh/Left/state_%04d.vtk",stepsvtkprefix.c_str() ,refstep);
	    movingvtkFilenameL="";
	    movingvtkFilenameL.append(bufferL);
        
	    char bufferL2[255];
	    sprintf(bufferL2,"%sMovingCTmesh/Left/state_%04d.vtk",stepsvtkprefix.c_str(),step);
	    fixedvtkFilenameL="";
	    fixedvtkFilenameL.append(bufferL2);
        
        char bufferR[255];
        sprintf(bufferR,"%sMovingCTmesh/Right/state_%04d.vtk",stepsvtkprefix.c_str(),refstep);
        movingvtkFilenameR="";
        movingvtkFilenameR.append(bufferR);
    
        char bufferR2[255];
        sprintf(bufferR2,"%sMovingCTmesh/Right/state_%04d.vtk",stepsvtkprefix.c_str(),step);
        fixedvtkFilenameR="";
        fixedvtkFilenameR.append(bufferR2);

	  }
	  else
	  {
            char bufferL[255];
            sprintf(bufferL,"%sMovingCTmesh/Left/state_%04d.vtk",stepsvtkprefix.c_str(),step);
            movingvtkFilenameL="";
            movingvtkFilenameL.append(bufferL);
        
            char bufferL2[255];
            sprintf(bufferL2,"%sMovingCTmesh/Left/state_%04d.vtk",stepsvtkprefix.c_str(),refstep);
            fixedvtkFilenameL="";
            fixedvtkFilenameL.append(bufferL2);
         
            char bufferR[255];
            sprintf(bufferR,"%sMovingCTmesh/Right/state_%04d.vtk",stepsvtkprefix.c_str(),step);
            movingvtkFilenameR="";
            movingvtkFilenameR.append(bufferR);
        
            char bufferR2[255];
            sprintf(bufferR2,"%sMovingCTmesh/Right/state_%04d.vtk",stepsvtkprefix.c_str(),refstep);
            fixedvtkFilenameR="";
            fixedvtkFilenameR.append(bufferR2);
	  }
	  
	printf("Fixed    %s\n",fixedvtkFilenameL.c_str());
	printf("Fixed    %s\n",fixedvtkFilenameR.c_str());
	printf("Moving   %s\n",movingvtkFilenameL.c_str());
	printf("Moving   %s\n",movingvtkFilenameR.c_str());    
    
	vtkSmartPointer<vtkPolyDataReader> fixedReaderL=vtkSmartPointer<vtkPolyDataReader>::New();
	fixedReaderL->SetFileName(fixedvtkFilenameL.c_str());
	fixedReaderL->Update();

	vtkSmartPointer<vtkPolyDataReader> movingReaderL=vtkSmartPointer<vtkPolyDataReader>::New();
	movingReaderL->SetFileName(movingvtkFilenameL.c_str());
	movingReaderL->Update();
        
    vtkSmartPointer<vtkPolyDataReader> fixedReaderR=vtkSmartPointer<vtkPolyDataReader>::New();
   	fixedReaderR->SetFileName(fixedvtkFilenameR.c_str());
   	fixedReaderR->Update();

    vtkSmartPointer<vtkPolyDataReader> movingReaderR=vtkSmartPointer<vtkPolyDataReader>::New();
    movingReaderR->SetFileName(movingvtkFilenameR.c_str());
    movingReaderR->Update();
    
	if (fixedReaderL->GetOutput()->GetNumberOfPoints() != movingReaderL->GetOutput()->GetNumberOfPoints())
	{
	  cout << "Lungs Left: Fixed and moving points must be the same number of points!" << endl;
	  exit(1);
	}
	
    	if (fixedReaderR->GetOutput()->GetNumberOfPoints() != movingReaderR->GetOutput()->GetNumberOfPoints())
    	{
    	  cout << "Lungs Right: Fixed and moving points must be the same number of points!" << endl;
    	  exit(1);
    	}
    
	long int pointid=0;
	PointSetType :: Pointer pointSet = PointSetType :: New ();
	PointSetType :: PointType point;
	VectorType V;

	cout << "Creating set of known motion vectors..." << endl;

	// create bounding box zero motion points
	int dx=20;
	int *roiedge=new int[3];

	for (roiedge[0]=0;roiedge[0]<=dx;++roiedge[0])
	{
	  for (roiedge[1]=0;roiedge[1]<=dx;++roiedge[1])
	  {
	    for (roiedge[2]=0;roiedge[2]<=dx;++roiedge[2])
	    {
	      if (roiedge[0]==0 || roiedge[0]==dx || roiedge[1]==0 || roiedge[1]==dx || roiedge[2]==0 || roiedge[2]==dx) // do create zero motion vectors only on the surface of the roi box
	      {
		for (int i=0;i<3;++i)
		{
		  point[i]=roibounds[2*i]+roiedge[i]*(roibounds[2*i+1]-roibounds[2*i])/dx;
		}
		V[0]=0;
		V[1]=0;
		V[2]=0;
		pointSet -> SetPoint ( pointid , point );
		pointSet -> SetPointData ( pointid , V );
		++pointid;
	      }
	    }
	  }
	}

	for (int i=0;i<fixedReaderL->GetOutput()->GetNumberOfPoints();++i)
	{
	  double *fixpoint=fixedReaderL->GetOutput()->GetPoint(i);
	  double *movpoint=movingReaderL->GetOutput()->GetPoint(i);

	  V[0]=movpoint[0]-fixpoint[0];
	  V[1]=movpoint[1]-fixpoint[1];
	  V[2]=movpoint[2]-fixpoint[2];
	  point[0]=fixpoint[0];
	  point[1]=fixpoint[1];
	  point[2]=fixpoint[2];

	  pointSet -> SetPoint ( pointid , point );
	  pointSet -> SetPointData ( pointid , V );
	  ++pointid;
	}

	
    for (int i=0;i<fixedReaderR->GetOutput()->GetNumberOfPoints();++i)
    {
      double *fixpoint=fixedReaderR->GetOutput()->GetPoint(i);
      double *movpoint=movingReaderR->GetOutput()->GetPoint(i);

      V[0]=movpoint[0]-fixpoint[0];
      V[1]=movpoint[1]-fixpoint[1];
      V[2]=movpoint[2]-fixpoint[2];
      point[0]=fixpoint[0];
      point[1]=fixpoint[1];
      point[2]=fixpoint[2];

      pointSet -> SetPoint ( pointid , point );
      pointSet -> SetPointData ( pointid , V );
      ++pointid;
    }
	return pointSet;
}


VectorImageType::Pointer ITK_to_VF(string body_part, int refstep, int step, int deformationOrMotionfield, string stepsvtkprefix, double *roibounds, VectorImageType::SizeType size, VectorImageType::PointType origin, VectorImageType::SpacingType spacing )
//The PointSet with displacement vectors is read in using the vtk meshes. Afterwards the PointSet is interpolated with BSplinne, yielding a VectorField.
{	
	PointSetType :: Pointer pointSet = PointSetType :: New ();
	if (body_part=="lungs"){ pointSet = CreateLungPointSet(refstep, step, deformationOrMotionfield, stepsvtkprefix, roibounds);}
    else if (body_part=="ribcage"){ pointSet = CreateRibcagePointSet(refstep, step, deformationOrMotionfield, stepsvtkprefix, roibounds);}
    
    
  	cout << "Interpolating with B-Spline between known motion vectors..." << endl;


	FilterType :: Pointer filter = FilterType :: New ();

	int no_splineorder=3;
	int no_cps=5;
	int noLevels=6;

	filter -> SetSize ( size );
	filter -> SetOrigin ( origin );
	filter -> SetSpacing ( spacing );
	filter -> SetInput ( pointSet );
	filter -> SetSplineOrder ( no_splineorder );
	FilterType :: ArrayType ncps ;
	ncps . Fill ( no_cps );
	filter -> SetNumberOfControlPoints ( ncps );
	filter -> SetNumberOfLevels ( noLevels );

	try
	  {
	  filter -> Update ();
	  }
	catch (itk::ExceptionObject &err)
	  {
	  std :: cerr << "Test 2: itkBSplineScatteredDataImageFilter exception thrown " << std :: endl ;
	  std::cout << err << std::endl;
	  exit (1);
	  }


	return filter->GetOutput();
}


FloatImageType::Pointer GetWarpedDistanceMap(FloatImageType::Pointer referenceMap, VectorImageType::Pointer DeformationField, string Prefix, string BodyPart, int step)
//The referenceMap is warped according to the DeformationField. The resulting distance map is saved in returned.
{	
	cout << "Warp the distance map" << endl;
	FloatWarperType::Pointer DistanceMap = FloatWarperType::New();
	FloatInterpolatorType::Pointer interpolator = FloatInterpolatorType::New();
	
	DistanceMap->SetInput( referenceMap );
	DistanceMap->SetInterpolator( interpolator );
	DistanceMap->SetOutputSpacing( referenceMap->GetSpacing() );
	DistanceMap->SetDisplacementField( DeformationField );
	DistanceMap->SetOutputOrigin( referenceMap->GetOrigin() );
	DistanceMap->Update();
	
	char buffer_map[255];
	sprintf(buffer_map, "%sDistanceMaps/%s_distanceMap_%04d.mha", Prefix.c_str(), BodyPart.c_str(), step);
	string DistanceMapFilename="";
	DistanceMapFilename.append(buffer_map);

	cout << "Save distance map at: " << DistanceMapFilename << endl;

	FloatImageWriterType::Pointer OutputWriter = FloatImageWriterType::New();
	OutputWriter->SetInput(DistanceMap->GetOutput());
	OutputWriter->SetFileName(DistanceMapFilename);
	OutputWriter->Update();
	OutputWriter->Write();
	return DistanceMap->GetOutput();
}


VectorImageType::PixelType GetDirection(VectorImageType::PixelType vector)
//The Direction-Vector of vector is calculated.
{
	VectorImageType::PixelType newVector;
	double length = sqrt( pow( vector[0] , 2.0) + pow( vector[1] , 2.0) + pow( vector[2] , 2.0));
	for (int j=0;j<3;++j)
	{
	  newVector[j]=vector[j] / length;
	}
	return newVector;
}

void printInfo(){
	//cout << endl << "Creates B-spline interpolated vector fields from fixed.vtk + moving.vtk + refimage.mha + roi extent (in slice numbers)" << endl;
	cout << endl << "The required objects (e.g. distanceMaps, gradients, uncorrected vectorfields for lungs and ribcage), which are needed for further steps, are generated and saved." << endl;	
	cout << "Usage: prepareSeparateVFs stepsvtkprefix refstep startstep stopstep refFilename (.mha) internalRegionFilename (.mha) xmin xmax ymin ymax zmin zmax " << endl << endl;
}
// 

int main (int argc, char** argv){	
	int refstep,startstep,stopstep;
	string stepsvtkprefix,refFilename;
	string internalRegionFilename;
	
	if (argc==1 || (argc<13)){
		printInfo();
		exit(1);
	}
	
	cout << "The required objects (e.g. distanceMaps, gradients, uncorrected vectorfields for lungs and ribcage), which are needed for further steps, are generated and saved. The correction of the of the vectorfields, together with the calculation of the warped timesteps are performed in a seperate script. Before running the second script, a diffusionMap has to be generated from the multiplied distance map (see further below). This is performed in the main matlab script." << endl;
	
	stepsvtkprefix=argv[1];
	refstep=atoi(argv[2]);
	startstep=atoi(argv[3]);
	stopstep=atoi(argv[4]);

	refFilename=argv[5];

	ImageMaskReaderType::Pointer internalReader=ImageMaskReaderType::New();
	internalRegionFilename=argv[6];
	internalReader->SetFileName(internalRegionFilename.c_str());
	internalReader->Update();
	
	VectorImageType::PixelType vectornomotion;
	vectornomotion.Fill(0);
	
	cout << "Loading .mha reference file..." << endl;

	ImageReaderType::Pointer itkReader=ImageReaderType::New();
	itkReader->SetFileName(refFilename.c_str());
	itkReader->Update();

	//Some Values needed for the BSpline Interpolation of the VTK PointSet	
	int *extent=new int[6];
	for (int i=0;i<6;++i)
	{
	  extent[i]=atoi(argv[7+i]);
	}
	
	double *roibounds=new double[6];
	for (int i=0;i<3;++i)
	{
	  roibounds[2*i]=extent[2*i]*itkReader->GetOutput()->GetSpacing()[i]+itkReader->GetOutput()->GetOrigin()[i];
	  roibounds[2*i+1]=extent[2*i+1]*itkReader->GetOutput()->GetSpacing()[i]+itkReader->GetOutput()->GetOrigin()[i];
	}

	VectorImageType::SizeType size;
	size[0]=extent[1]-extent[0]+1;
	size[1]=extent[3]-extent[2]+1;
	size[2]=extent[5]-extent[4]+1;

	VectorImageType::PointType origin;
	origin[0]=roibounds[0];
	origin[1]=roibounds[2];
	origin[2]=roibounds[4];
	
	VectorImageType::SpacingType spacing;
	spacing[0]=(roibounds[1]-roibounds[0])/(size[0]-1);
	spacing[1]=(roibounds[3]-roibounds[2])/(size[1]-1);
	spacing[2]=(roibounds[5]-roibounds[4])/(size[2]-1);
	
	VectorImageType::SizeType outsize= itkReader->GetOutput()->GetLargestPossibleRegion().GetSize();
	
	cout << "Prepare original distance function" << endl;
	char buffer_map[255];
	sprintf(buffer_map,"%sDistanceMaps/OriginalDistanceMap.mha",stepsvtkprefix.c_str());
	string OrigMapFilename="";
	OrigMapFilename.append(buffer_map);
	
	DistanceMap::Pointer OriginalDistanceMap = DistanceMap::New();
	OriginalDistanceMap->SetInput(internalReader->GetOutput());
    OriginalDistanceMap->SetInsideIsPositive(true); //shifted by spacing of voxel compared to using SetInsideIsPositive(false) and externalMask
	OriginalDistanceMap->Update();
	
	FloatImageWriterType::Pointer OrigDistanceWriter = FloatImageWriterType::New();
	OrigDistanceWriter->SetFileName(OrigMapFilename);
	OrigDistanceWriter->SetInput(OriginalDistanceMap->GetOutput());
	OrigDistanceWriter->Update();
	OrigDistanceWriter->Write();
	OrigDistanceWriter = NULL;
    cout << "Original distance map saved at " << OrigMapFilename << endl;
    
	cout << "Prepare gradient of original distance function ";
	GradientFilterType::Pointer OriginalGradientFilter = GradientFilterType::New();
	OriginalGradientFilter->SetInput(OriginalDistanceMap->GetOutput());
	OriginalGradientFilter->Update();
	cout << "and save it as VectorImage" << endl;
	
	FloatImageType::Pointer OriginalGradient [3];
	for (int j=0;j<3;++j)
	{
	  OriginalGradient[j]=FloatImageType::New();
	  OriginalGradient[j]->SetRegions(itkReader->GetOutput()->GetLargestPossibleRegion());
	  OriginalGradient[j]->SetSpacing(itkReader->GetOutput()->GetSpacing());
	  OriginalGradient[j]->SetOrigin(itkReader->GetOutput()->GetOrigin());
	  OriginalGradient[j]->Allocate();
	}
	
	for (unsigned int x=0;x<outsize[0];++x)
	{
	  for (unsigned int y=0;y<outsize[1];++y)
	    {
	      for (unsigned int z=0;z<outsize[2];++z)
		{
		  VectorImageType::IndexType gradindex;
		  gradindex[0]=x;
		  gradindex[1]=y;
		  gradindex[2]=z;
		  VectorImageType::PixelType GradientVector;
		  for (int j=0;j<3;++j)
		  {
		    GradientVector[j]=OriginalGradientFilter->GetOutput()->GetPixel(gradindex)[j];
		  }
		  GradientVector = GetDirection(GradientVector);
		  for (int j=0;j<3;++j)
		  {
		    OriginalGradient[j]->SetPixel(gradindex, GradientVector[j]);
		  }
		  
	}}}
	
	for (int j=0;j<3;++j)
	{ 
	  char buffer_grad[255];
	  sprintf(buffer_grad,"%sDistanceMaps/OriginalGradient_%d.mha",stepsvtkprefix.c_str(),j);
	  string GradFilename="";
	  GradFilename.append(buffer_grad);
	  cout << "Gradient Direction " << j << " saved at " << GradFilename << endl;
	  
	  FloatImageWriterType::Pointer OrigGradWriter = FloatImageWriterType::New();
	  OrigGradWriter->SetInput(OriginalGradient[j]);
	  OrigGradWriter->SetFileName(GradFilename.c_str());
	  OrigGradWriter->Update();
	  OrigGradWriter->Write();
	}

	for (int j=0;j<3;++j)
	{
	  OriginalGradient[j] = NULL;
	}

	//loop over all steps begins
	for (int step=startstep;step<=stopstep;step+=1)
	{   
        printf("Timestep %d\n",step);
        
        for (int deformationOrMotionfield=0;deformationOrMotionfield<=1;deformationOrMotionfield+=1){
            string lungVfOutFilename="";
            string ribcageVfOutFilename="";
        
            if (deformationOrMotionfield==0){ // deformation vector field
                char bufferVfLung[255];
                sprintf(bufferVfLung,"%sMotionVectors/lungs/deformationfield_%04d.mha",stepsvtkprefix.c_str(),step);
                lungVfOutFilename="";
                lungVfOutFilename.append(bufferVfLung);
                char bufferVfRibcage[255];
                sprintf(bufferVfRibcage,"%sMotionVectors/ribcage/deformationfield_%04d.mha",stepsvtkprefix.c_str(),step);
                ribcageVfOutFilename="";
                ribcageVfOutFilename.append(bufferVfRibcage);
                printf("Output Lung Deformationfield   %s\n",lungVfOutFilename.c_str());
                printf("Output Ribcage Deformationfield   %s\n",ribcageVfOutFilename.c_str());
            }
            else { // motion vector field
                char bufferVfLung[255];
                sprintf(bufferVfLung,"%sMotionVectors/lungs/motionfield_%04d.mha",stepsvtkprefix.c_str(),step);
                lungVfOutFilename="";
                lungVfOutFilename.append(bufferVfLung);
                char bufferVfRibcage[255];
                sprintf(bufferVfRibcage,"%sMotionVectors/ribcage/motionfield_%04d.mha",stepsvtkprefix.c_str(),step);
                ribcageVfOutFilename="";
                ribcageVfOutFilename.append(bufferVfRibcage);
                printf("Output Lung Motionfield   %s\n",lungVfOutFilename.c_str());
                printf("Output Ribcage Motionfield   %s\n",ribcageVfOutFilename.c_str());                
            }
  
	        //The Vectorfields are calculated from the meshes with BSpline Interpolation. However, doing so, certain vectors can point outside of the image region.
            
            cout << "Lung ITK to VF" << endl; 
            VectorImageType::Pointer LungVectorfieldBasic = ITK_to_VF( "lungs", refstep,  step, deformationOrMotionfield, stepsvtkprefix, roibounds, size,  origin, spacing );
            cout << "Ribcage ITK to VF" << endl;
            VectorImageType::Pointer RibcageVectorfieldBasic = ITK_to_VF( "ribcage", refstep,  step, deformationOrMotionfield, stepsvtkprefix, roibounds, size,  origin, spacing );
            cout << "Saving output vector field..." << endl;
            //Vectorfield from Lung
            VectorImageType::Pointer LungVectorfield=VectorImageType::New();
            LungVectorfield->SetRegions(itkReader->GetOutput()->GetLargestPossibleRegion());
            LungVectorfield->SetSpacing(itkReader->GetOutput()->GetSpacing());
            LungVectorfield->SetOrigin(itkReader->GetOutput()->GetOrigin());
            LungVectorfield->Allocate();
            LungVectorfield->FillBuffer(vectornomotion);
            //Vectorfield from Ribcage 
            VectorImageType::Pointer RibcageVectorfield=VectorImageType::New();
            RibcageVectorfield->SetRegions(itkReader->GetOutput()->GetLargestPossibleRegion());
            RibcageVectorfield->SetSpacing(itkReader->GetOutput()->GetSpacing());
            RibcageVectorfield->SetOrigin(itkReader->GetOutput()->GetOrigin());
            RibcageVectorfield->Allocate();
            RibcageVectorfield->FillBuffer(vectornomotion);
	
            if (LungVectorfield->GetLargestPossibleRegion() != RibcageVectorfield->GetLargestPossibleRegion()) { cout << "Lung and Ribcage Vectorfield have different Regions" << endl; exit(1);}

            //The vectors of the vectorfields are constrained to the image region.
            for (unsigned int x=0;x<size[0];++x)
            {
            for (unsigned int y=0;y<size[1];++y)
                {
                for (unsigned int z=0;z<size[2];++z)
                {
                VectorImageType::IndexType filterindex;
                filterindex[0]=x;
                filterindex[1]=y;
                filterindex[2]=z;
                VectorImageType::IndexType outputindex;
                outputindex[0]=filterindex[0]+extent[0];
                outputindex[1]=filterindex[1]+extent[2];
                outputindex[2]=filterindex[2]+extent[4];
                
                if (LungVectorfield->GetLargestPossibleRegion().IsInside(outputindex)) {                        
                LungVectorfield->SetPixel(outputindex, ConstrainVectorToRegion(LungVectorfield->GetLargestPossibleRegion(), LungVectorfieldBasic->GetPixel(filterindex), outputindex, spacing));
                RibcageVectorfield->SetPixel(outputindex, ConstrainVectorToRegion(RibcageVectorfield->GetLargestPossibleRegion(), RibcageVectorfieldBasic->GetPixel(filterindex), outputindex, spacing));
                }
                }
                }
            }
            LungVectorfieldBasic = NULL;
            RibcageVectorfieldBasic = NULL;
	
            //The individual fields are saved with the Filenames created further above. This is needed for further steps, when the fields are combined.
            cout << "Save the individual vectorfields" << endl;
            VectorImageWriterType::Pointer lungOutwriter = VectorImageWriterType::New();
            lungOutwriter->SetInput(LungVectorfield);
            lungOutwriter->SetUseCompression(true);     //Compressed files cannot be read by Matlab. As long as we do not need the Vectorfield its fine
            lungOutwriter->SetFileName(lungVfOutFilename.c_str());
            lungOutwriter->Update();
            lungOutwriter->Write();
            lungOutwriter =NULL;

            VectorImageWriterType::Pointer ribcageOutwriter=VectorImageWriterType::New();
            ribcageOutwriter->SetInput(RibcageVectorfield);
            ribcageOutwriter->SetUseCompression(true);     //Compressed files cannot be read by Matlab.
            ribcageOutwriter->SetFileName(ribcageVfOutFilename.c_str());
            ribcageOutwriter->Update();
            ribcageOutwriter->Write();
            ribcageOutwriter = NULL;

            
            if (deformationOrMotionfield==1){	
                //The original DistanceMap (created above) is warped with the lungs and ribcage motionfields seperately.
                FloatImageType::Pointer LungDistanceMap = GetWarpedDistanceMap(OriginalDistanceMap->GetOutput(), LungVectorfield, stepsvtkprefix ,"lung", step);
                FloatImageType::Pointer RibcageDistanceMap = GetWarpedDistanceMap(OriginalDistanceMap->GetOutput(), RibcageVectorfield, stepsvtkprefix, "ribcage", step);
                
                LungVectorfield = NULL;
                RibcageVectorfield = NULL;
                
                //The Multplication and Addition of the DistanceMap is needed for defining the gap/overlap-area and the new average distance map
                cout << "Calculate the Multiplication and Addition of the Distance Map" << endl;
                MultiplyFilter::Pointer Multiply = MultiplyFilter::New();
                Multiply->SetInput1(LungDistanceMap);
                Multiply->SetInput2(RibcageDistanceMap);
                Multiply->Update();

                AddImageFilter::Pointer addFilter = AddImageFilter::New();
                addFilter->SetInput1(LungDistanceMap);
                addFilter->SetInput2(RibcageDistanceMap);
                addFilter->Update();
                MultiplyFilter::Pointer addRescaled = MultiplyFilter::New();
                addRescaled->SetInput(addFilter->GetOutput());
                addRescaled->SetConstant(0.5);
                
                cout << "Save the multiplied and added Distance Maps" << endl;
                char buffer_multiply[255];
                sprintf(buffer_multiply, "%sDistanceMaps/multiplied_distanceMap_%04d.mha", stepsvtkprefix.c_str(),  step);
                string MultiplyDistanceMapFilename="";
                MultiplyDistanceMapFilename.append(buffer_multiply);
                char buffer_add[255];
                sprintf(buffer_add, "%sDistanceMaps/added_distanceMap_%04d.mha", stepsvtkprefix.c_str(),  step);
                string AddDistanceMapFilename="";
                AddDistanceMapFilename.append(buffer_add);
                cout << "Save Multiplied Map at " << MultiplyDistanceMapFilename << endl;
                cout << "Save Added and Rescaled Map at " << AddDistanceMapFilename << endl;


                FloatImageWriterType::Pointer MultiplyOutputWriter = FloatImageWriterType::New();
                MultiplyOutputWriter->SetInput(Multiply->GetOutput());
                MultiplyOutputWriter->SetFileName(MultiplyDistanceMapFilename);
                MultiplyOutputWriter->Update();
                MultiplyOutputWriter->Write();

                FloatImageWriterType::Pointer rescaledAddOutputWriter = FloatImageWriterType::New();
                rescaledAddOutputWriter->SetInput(addRescaled->GetOutput());
                rescaledAddOutputWriter->SetFileName(AddDistanceMapFilename);
                rescaledAddOutputWriter->Update();
                rescaledAddOutputWriter->Write();
            } 
        } //MVF and DVF
	
	} // end for each timestep
	cout << "done !" << endl;
}


