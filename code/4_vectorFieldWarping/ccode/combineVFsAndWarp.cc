#include <vtkSmartPointer.h>
#include <vtkPolyDataWriter.h>
#include <vtkPolyDataReader.h>
#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkDelaunay3D.h>
#include <vtkUnstructuredGrid.h>
#include <vtkTetra.h>

#include <itkImage.h>
#include <itkVector.h>
#include <itkImageFileReader.h>
#include <itkImageFileWriter.h>
#include <itkLinearInterpolateImageFunction.h>
#include <itkNearestNeighborInterpolateImageFunction.h>
#include <itkWarpImageFilter.h>
#include <itkImageDuplicator.h> 

#include <cstdlib>
#include <cstdio>

#include <itkPointSet.h>
#include <itkBSplineScatteredDataPointSetToImageFilter.h>

#include "itkCastImageFilter.h"
#include <itkMultiplyImageFilter.h>
#include <itkAddImageFilter.h>

#include "itkSignedDanielssonDistanceMapImageFilter.h"
#include <itkImageRegionIterator.h>
#include <itkVectorLinearInterpolateImageFunction.h>
#include "itkGradientImageFilter.h"
#include <itkDiscreteGaussianImageFilter.h>
#include <itkRecursiveGaussianImageFilter.h>
#include "itkBinaryThresholdImageFilter.h"
#include "itkSubtractImageFilter.h"
#include <itkInverseDisplacementFieldImageFilter.h>
#include <itkInvertDisplacementFieldImageFilter.h>
#include <itkInvertDisplacementFieldImageFilter.h>
#include <itkDisplacementFieldJacobianDeterminantFilter.h>
	
typedef itk::Vector<double, 3> VectorType;
typedef itk::Image<VectorType, 3> VectorImageType;
typedef itk::Image<short, 3> ImageType;
typedef itk::Image<unsigned char, 3> ImageMaskType;
typedef itk::ImageFileReader<ImageType> ImageReaderType;
typedef itk::ImageFileWriter<ImageType> ImageWriterType;
typedef itk::ImageFileReader<VectorImageType> VectorImageReaderType;
typedef itk::ImageFileWriter<VectorImageType> VectorImageWriterType;
typedef itk::ImageFileReader<ImageMaskType> ImageMaskReaderType;

typedef itk::ImageDuplicator< ImageType > DuplicatorType;

typedef itk::Point< float, 3 > FloatPoint3DType;

// needed for no rounding errors when warping the image
typedef itk::Image<float, 3> FloatImageType;
typedef itk::ImageFileReader<FloatImageType> FloatImageReaderType;
typedef itk::WarpImageFilter<FloatImageType, FloatImageType, VectorImageType >  FloatWarperType;
typedef itk::LinearInterpolateImageFunction<FloatImageType, double >  FloatInterpolatorType;
typedef itk::CastImageFilter<ImageType, FloatImageType> ChangeFilterType_inttofloat;
typedef itk::CastImageFilter<FloatImageType, ImageType> ChangeFilterType_floattoint;

typedef itk::WarpImageFilter<ImageMaskType, ImageMaskType, VectorImageType >  MaskWarperType;
typedef itk::NearestNeighborInterpolateImageFunction<ImageMaskType, double >  MaskInterpolatorType;

//Linear Interpolator for Vector Fields (or VectorImages)
typedef itk::VectorLinearInterpolateImageFunction <VectorImageType, float > VectorInterpolatorType;
typedef itk::RecursiveGaussianImageFilter< VectorImageType, VectorImageType > VectorGaussianSmoothingFilter;
typedef itk::SubtractImageFilter<FloatImageType, FloatImageType> SubtractImageFilter;
typedef itk::DisplacementFieldJacobianDeterminantFilter< VectorImageType, float, FloatImageType > JacFilterType;

using namespace std;

int VectorNotZero(VectorImageType::PixelType vector)
{
	float length = vector[1]*vector[1] + vector[2]*vector[2] + vector[0]*vector[0];
	return length >0;
}


VectorImageType::PixelType DefCorrection(VectorImageType::Pointer Field, VectorImageType::IndexType& index, FloatImageType::Pointer Diffusor, VectorInterpolatorType::Pointer InterpolatedCorrection)
//Add the Corection to the Deformation fields. The motionfield correction is interpolated. For every voxel the endpoint of the deformationfield is calcualted. This point is then used to evaluate the interpolated
//motionfield correction at that point. The negative of the correction is then used to alter the starting point of the deformation vector. 
{
	//Negative of correction!!!!!
	VectorImageType::PixelType vector = Field->GetPixel(index);
	VectorImageType::SpacingType spacing = Field->GetSpacing();
	FloatPoint3DType origin = Field->GetOrigin();
	VectorImageType::IndexType pointing_to_index;
	FloatPoint3DType point;
	for (int j=0;j<3;++j)
	{
	  point[j]=origin[j] + index[j]*spacing[j] +vector[j];
	  pointing_to_index[j]=index[j]+static_cast<int>(round(vector[j]/spacing[j]));
	}
	
	if (InterpolatedCorrection->IsInsideBuffer(point) && Field->GetLargestPossibleRegion().IsInside(pointing_to_index))
	{
		if (Diffusor->GetPixel(pointing_to_index) != 0)
		{
			VectorImageType::PixelType CorrectionVector = InterpolatedCorrection->Evaluate (point);
			VectorImageType::IndexType Corrected_index;
			for (int j=0;j<3;++j)
			{
		  	   vector[j]-= CorrectionVector[j];
			   Corrected_index[j] = index[j] - static_cast<int>(round(CorrectionVector[j]/spacing[j]));
			}
			if (! Field->GetLargestPossibleRegion().IsInside(Corrected_index))
			{
			  VectorImageType::PixelType NewVector;
			  double fraction_of_vector=0.95;
			  while (fraction_of_vector>0) {
			    for (int j=0;j<3;++j)
			    {
				NewVector[j]=CorrectionVector[j]*fraction_of_vector;
				Corrected_index[j] = index[j] - static_cast<int>(round(NewVector[j]/spacing[j]));
				Corrected_index[j] = index[j] - static_cast<int>(round(fraction_of_vector*CorrectionVector[j]/spacing[j]));
			    }
			    if (Field->GetLargestPossibleRegion().IsInside(Corrected_index)){ break;}
			    fraction_of_vector-=0.05;
			  }
		}
		
			index = Corrected_index;
		}
	}
	return vector;
}
VectorImageType::PixelType NearestNeighbourAverage(VectorImageType::Pointer Field, VectorImageType::IndexType outindex, VectorImageType::PixelType OldVector, ImageType::Pointer Indicator)
//Unweighted nearest neighbur average over the new deformationfield. If less than three neighbours are occupied, the old vector is also included in the average
{
	VectorImageType::PixelType OutVector;
	for (int j=0;j<3;++j)
	{
	   OutVector[j] = 0;
	}
	int counter = 0;
	for (int j=0;j<3;++j)
	{
	   for (int i=0;i<2;++i)
	   {
		VectorImageType::IndexType NN_Index = outindex;
		NN_Index[j] += pow((-1), i);
		if (Field->GetLargestPossibleRegion().IsInside(NN_Index))
		{

			VectorImageType::PixelType NN_Vector = Field->GetPixel(NN_Index);
			if (VectorNotZero(NN_Vector) && (Indicator->GetPixel(NN_Index)>0))
			{
				OutVector[0] = (counter*OutVector[0] + NN_Vector[0])/(counter+1);
				OutVector[1] = (counter*OutVector[1] + NN_Vector[1])/(counter+1);
				OutVector[2] = (counter*OutVector[2] + NN_Vector[2])/(counter+1);
				counter++;
			}			
		}		
	   }
	}

	if (counter <3)
	{
		OutVector[0] = (counter*OutVector[0] + OldVector[0])/(counter+1);
		OutVector[1] = (counter*OutVector[1] + OldVector[1])/(counter+1);
		OutVector[2] = (counter*OutVector[2] + OldVector[2])/(counter+1);
	}
	return OutVector;
}




float density_calc(short HU, float det, string corr_type)
//Depending on the initial voxel value, the HU-intensity scaling is applied differetnly. More rigid voxels, with larger HU, should not get as much of a HU-intensity scaling as 'soft' voxels
{
	short HU_prime;
	float w=0; // default no HU-intensity scaling

	if (corr_type=="linear1") { w=(HU+1000)/750*(HU<-250)+(HU>=-250);} //different linear HU-intensity scaling	
	else if (corr_type=="linear2") {w=(HU+1000)/800*(HU<-200)+(HU>=-200);}
	else if (corr_type=="linear3") { w=(HU+1000)/850*(HU<-150)+(HU>=-150);}
	else if (corr_type=="cutoff1") {w=(HU>=-250);} //cutoffs
	else if (corr_type=="cutoff2") {w=(HU>=-200);}
	else if (corr_type=="cutoff3") {w=(HU>=-150);}
	else if (corr_type=="nodenschange") {w=1;}
	HU_prime = (HU+1000)*(1/det*(1-w)+w)-1000; //one option but clearly not mass conserving

	return HU_prime;
}

void ImplementDensityChanges(ImageType::Pointer& CT_copy, VectorImageType::Pointer Vectorfield, ImageMaskType::Pointer LungMask)
//The HU intensities are scaled according to the Determinant of the Jacobian of the Deformation field. The scaling is only applied for voxels in the lungs and with 1/3<detJ<3 (otherwhise unhysical)
{
	  string corr_type = "cutoff3";//Default see comment on functiondensity_calc above
	  float Jac_BoundLow = 1/3.;
	  float Jac_Bound_high = 3.;
	  cout << "Calculate the Jacobian of the deformation field" << endl;
	  JacFilterType::Pointer Jacobian = JacFilterType::New();
    	  Jacobian->SetInput(Vectorfield);
    	  Jacobian->Update();
	  FloatImageType::RegionType region = Jacobian->GetOutput()->GetLargestPossibleRegion();
	  FloatImageType::SizeType insize = region.GetSize();

	  cout << "Calculate the HU-intensity scaling and the problem mask" << endl;
	  
	  for (unsigned int x=0; x<insize[0];x++)
	    {
	  for (unsigned int y=0; y<insize[1];y++)
	    {
	  for (unsigned int z=0; z<insize[2];z++)
	    {
	    ImageType::IndexType densityIndex;
	    densityIndex[0]=x;
	    densityIndex[1]=y;
	    densityIndex[2]=z;
	    FloatImageType::PixelType Jac = Jacobian->GetOutput()->GetPixel(densityIndex);

	    if ((LungMask->GetPixel(densityIndex)>0 ))//Only points within lungs experience HU-intensity scaling
		{
	    	if (Jac>Jac_BoundLow && Jac<Jac_Bound_high )//exclude big HU-intensity scaling, can change limits -> in 4DCT det(Jacobian) in [0.5,1.66] -> set limits at beginning-> set limits to 1/3 and 3
		  {
		    short old_val=CT_copy->GetPixel(densityIndex);
		    short new_val;
		    new_val= density_calc(old_val,Jac, corr_type);
		    CT_copy->SetPixel(densityIndex, new_val);
		  }	    	
		}
	    }}}
}



void WarpNWrite(ImageType::Pointer referenceImage, VectorImageType::Pointer Vectorfield, string output_name)
{	//When warping the image with PixelType short, rounding errors can occur (float to short) somewhere in the Process/BlackBox of Warping and following Interpolation. This even happens for a displacement field full of 0-vectors.
	//To avoid these errors the Input Image is first converted to float, then warped and afterwards converted back to short.

	ChangeFilterType_inttofloat::Pointer inputtofloat = ChangeFilterType_inttofloat::New();
	inputtofloat->SetInput(referenceImage);
	
	FloatWarperType::Pointer warper = FloatWarperType::New();
	FloatInterpolatorType::Pointer interpolator = FloatInterpolatorType::New();
	
	warper->SetInput( inputtofloat->GetOutput() );
	warper->SetInterpolator( interpolator );
	warper->SetOutputSpacing( referenceImage->GetSpacing() );
	warper->SetDisplacementField( Vectorfield );
	warper->SetOutputOrigin( referenceImage->GetOrigin() );
	
	ChangeFilterType_floattoint::Pointer floattofinal = ChangeFilterType_floattoint::New();
	floattofinal->SetInput(warper->GetOutput());

	ImageWriterType::Pointer warpedwriter = ImageWriterType::New();
	warpedwriter->SetInput(floattofinal->GetOutput());
	cout << "Final Output Name: " << output_name << endl;
	warpedwriter->SetFileName(output_name.c_str());
	warpedwriter->Update();
	warpedwriter->Write();
}

void printInfo(){
	//cout << endl << "Creates B-spline interpolated vector fields from fixed.vtk + moving.vtk + refimage.mha + roi extent (in slice numbers)" << endl;
	cout << endl << "The lung and ribcage deformation-/motionFields are combined including a correction term. Afterwards they are used to generate a 4DCT(MRI) including sliding organ motion along the ribcage and HU-intensity scaling inside the lungs." << endl;	
	cout << "Usage: Final_Run_IncludeRibcageMotion prefix refstep startstep stopstep reffilename (.mha) internalMaskFilename (.mha) lungMaskFilename (.mha) extendedBodyMaskFilename (.mha) " << endl << endl;
}

int main (int argc, char** argv){	

    int refstep,startstep,stopstep;
    string prefix,reffilename;
    string internalMaskFilename, lungMaskFilename;
    string extendedBodyMaskFilename;
    
    float Sigma_Value = 4.0;

    if (argc==1 || (argc<10)){
	printInfo();	
	cout << "argc = " << argc << "/10" << endl;
	exit(1);
    }

    cout << "Generate a 4DCT(CT/MRI) including the ribcage motion and density correction" << endl;
    cout << "The gaps and overlaps were corrected and the new, correct motionfields combined and inverted" << endl;
    cout << "Additionally the HU-intensity scaling inside the lung are implemented. For this the formalism with the determinant of the deformationfield is used. The deformationfield is found by inverting the corrected lung motionfield. " << endl;
    cout << endl;
    cout << endl;
    prefix=argv[1];
    refstep=atoi(argv[2]);
    startstep=atoi(argv[3]);
    stopstep=atoi(argv[4]);

    reffilename=argv[5];
    internalMaskFilename = argv[6];
    lungMaskFilename = argv[7];
    extendedBodyMaskFilename = argv[8];
    int DiffSize = atoi(argv[9]);
    
    cout << "Number of diffusion steps is " << DiffSize << endl;

    cout << "Loading reference CT from " << reffilename << endl << endl;
    ImageReaderType::Pointer itkreader=ImageReaderType::New();
    itkreader->SetFileName(reffilename.c_str());
    itkreader->Update();
    VectorImageType::SizeType outsize= itkreader->GetOutput()->GetLargestPossibleRegion().GetSize();

    cout << "Loading extended body mask from " << extendedBodyMaskFilename << endl << endl;
    ImageMaskReaderType::Pointer BodyMaskReader = ImageMaskReaderType::New();
    BodyMaskReader->SetFileName(extendedBodyMaskFilename.c_str());
    BodyMaskReader->Update();
    
    cout << "Loading internal mask from " << internalMaskFilename << endl << endl;
    ImageMaskReaderType::Pointer InternalMask = ImageMaskReaderType::New();
    InternalMask->SetFileName(internalMaskFilename.c_str());
    InternalMask->Update();

    cout << "Loading lung mask from " << lungMaskFilename << endl << endl; //for density changes
    ImageMaskReaderType::Pointer LungMaskReader = ImageMaskReaderType::New();
    LungMaskReader->SetFileName(lungMaskFilename.c_str());
    LungMaskReader->Update();
    
    VectorImageType::PixelType vectornomotion;
    vectornomotion.Fill(0);
    
    FloatImageReaderType::Pointer Orig_Grad_Reader [3];
    for (int j=0;j<3;++j)
    { 
	char buffer_grad[255];
	sprintf(buffer_grad,"%sDistanceMaps/OriginalGradient_%d.mha",prefix.c_str(),j);
	string GradFilename="";
	GradFilename.append(buffer_grad);
	cout << "Gradient Direction " << j << " from " << GradFilename << endl;
	  
	Orig_Grad_Reader[j] = FloatImageReaderType::New();
	Orig_Grad_Reader[j]->SetFileName(GradFilename.c_str());
	Orig_Grad_Reader[j]->Update();
    }
                   
    
    for (int step=startstep;step<=stopstep;step+=1){
        
        printf("Timestep %d\n",step);
        //declarations for objects used by both motion and deformation field steps
        VectorInterpolatorType::Pointer RibcageCorrectionInterpolator = VectorInterpolatorType::New();        
        VectorInterpolatorType::Pointer LungCorrectionInterpolator = VectorInterpolatorType::New();
        VectorImageType::Pointer combinedMotionfield=VectorImageType::New();
        
        //The diffusor is read in
        char buffer_diffusor[255];
        sprintf(buffer_diffusor,"%sDistanceMaps/diffusor_steps%d_%04d.mha",prefix.c_str(),DiffSize,step);
        string diffusor_filename="";
        diffusor_filename.append(buffer_diffusor);
        cout << "Diffusor from " << diffusor_filename << endl;
        FloatImageReaderType::Pointer DiffusorMap = FloatImageReaderType::New();
        DiffusorMap->SetFileName(diffusor_filename.c_str());
        DiffusorMap->Update();
        FloatImageType::Pointer Diffusor = DiffusorMap->GetOutput();
        
        for (int deformationORmotionfield=1;deformationORmotionfield>=0;deformationORmotionfield-=1){ //separately to save some memory, however, motionfield correction
            // has to be calculated first and some results are then used for deformationfield correction
            string lung_vf_filename="";
            string ribcage_vf_filename="";
        
            if (deformationORmotionfield==1){ // motion vector field first
                cout << "Read in motionfields " << endl;
                char bufferLung_field[255];
                sprintf(bufferLung_field,"%sMotionVectors/lungs/motionfield_%04d.mha",prefix.c_str(),step);
                lung_vf_filename="";
                lung_vf_filename.append(bufferLung_field);
                char bufferRibcage_field[255];
                sprintf(bufferRibcage_field,"%sMotionVectors/ribcage/motionfield_%04d.mha",prefix.c_str(),step);
                ribcage_vf_filename="";
                ribcage_vf_filename.append(bufferRibcage_field);
            }
            
            else {
                cout << "Read in deformationfields " << endl;
                char bufferLung_field[255];
                sprintf(bufferLung_field,"%sMotionVectors/lungs/deformationfield_%04d.mha",prefix.c_str(),step);
                lung_vf_filename="";
                lung_vf_filename.append(bufferLung_field);
                char bufferRibcage_field[255];
                sprintf(bufferRibcage_field,"%sMotionVectors/ribcage/deformationfield_%04d.mha",prefix.c_str(),step);
                ribcage_vf_filename="";
                ribcage_vf_filename.append(bufferRibcage_field);
            }

            cout << "Ribcage vectorfield from " << ribcage_vf_filename << endl;
            cout << "Lung vectorfield from " << lung_vf_filename << endl;
            
            VectorImageReaderType::Pointer RibcageFieldReader = VectorImageReaderType::New();
            RibcageFieldReader->SetFileName(ribcage_vf_filename.c_str());
            RibcageFieldReader->Update();
            VectorImageReaderType::Pointer LungFieldReader = VectorImageReaderType::New();
            LungFieldReader->SetFileName(lung_vf_filename.c_str());
            LungFieldReader->Update();

            cout << "Smooth the fields" << endl;
            VectorGaussianSmoothingFilter::Pointer RibcageFieldSmoother = VectorGaussianSmoothingFilter::New();
            RibcageFieldSmoother->SetSigma(Sigma_Value);
            RibcageFieldSmoother->SetInput(RibcageFieldReader->GetOutput());
            RibcageFieldSmoother->Update();
            VectorGaussianSmoothingFilter::Pointer LungFieldSmoother = VectorGaussianSmoothingFilter::New();
            LungFieldSmoother->SetSigma(Sigma_Value);
            LungFieldSmoother->SetInput(LungFieldReader->GetOutput());
            LungFieldSmoother->Update();

            VectorImageType::Pointer RibcageField = RibcageFieldSmoother->GetOutput();
            VectorImageType::Pointer LungField = LungFieldSmoother->GetOutput();            
            
            if (deformationORmotionfield==1){ // motion vector field first      
                cout << "Combine the motionfields" << endl;
                cout << "Read in Maps" << endl;
                char bufferLung_map[255];
                sprintf(bufferLung_map,"%sDistanceMaps/lung_distanceMap_%04d.mha",prefix.c_str(),step);
                string lung_map_filename="";
                lung_map_filename.append(bufferLung_map);
                char bufferRibcage_map[255];
                sprintf(bufferRibcage_map,"%sDistanceMaps/ribcage_distanceMap_%04d.mha",prefix.c_str(),step);
                string ribcage_map_filename="";
                ribcage_map_filename.append(bufferRibcage_map);
                char buffer_add_map[255];
                sprintf(buffer_add_map,"%sDistanceMaps/added_distanceMap_%04d.mha",prefix.c_str(),step);
                string add_map_filename="";
                add_map_filename.append(buffer_add_map);
                
                cout << "Ribcage Map from " << ribcage_map_filename << endl;
                cout << "Lung Map from " << lung_map_filename << endl;
                cout << "Add Map from " << add_map_filename << endl;

                FloatImageReaderType::Pointer RibcageMap = FloatImageReaderType::New();
                RibcageMap->SetFileName(ribcage_map_filename.c_str());
                RibcageMap->Update();
                FloatImageReaderType::Pointer LungMap = FloatImageReaderType::New();
                LungMap->SetFileName(lung_map_filename.c_str());
                LungMap->Update();

                FloatImageReaderType::Pointer AddMap = FloatImageReaderType::New();
                AddMap->SetFileName(add_map_filename.c_str());
                AddMap->Update();		

                SubtractImageFilter::Pointer SubMapLung = SubtractImageFilter::New();
                SubMapLung->SetInput1(LungMap->GetOutput());
                SubMapLung->SetInput2(AddMap->GetOutput());
                SubMapLung->Update();
                
                SubtractImageFilter::Pointer SubMapRibcage = SubtractImageFilter::New();
                SubMapRibcage->SetInput1(RibcageMap->GetOutput());
                SubMapRibcage->SetInput2(AddMap->GetOutput());
                SubMapRibcage->Update();

                cout << "Warp the Gradient with Lung Field" << endl;
                FloatWarperType::Pointer LungGradientWarper [3];
                FloatInterpolatorType::Pointer AllLung_GradInterpolator [3];

                for (int j=0;j<3;++j)
                {
                LungGradientWarper[j] = FloatWarperType::New();
                AllLung_GradInterpolator[j] = FloatInterpolatorType::New();
                
                LungGradientWarper[j]->SetInput( Orig_Grad_Reader[j]->GetOutput() );
                LungGradientWarper[j]->SetInterpolator( AllLung_GradInterpolator[j] );
                LungGradientWarper[j]->SetOutputSpacing( itkreader->GetOutput()->GetSpacing() );
                LungGradientWarper[j]->SetDisplacementField( LungField );
                LungGradientWarper[j]->SetOutputOrigin( itkreader->GetOutput()->GetOrigin() );
                LungGradientWarper[j]->Update();
                }

                cout << "Warp the Gradient with Ribcage Field" << endl;
                FloatWarperType::Pointer RibcageGradientWarper [3];
                FloatInterpolatorType::Pointer AllRibcage_GradInterpolator [3];

                for (int j=0;j<3;++j)
                {
                RibcageGradientWarper[j] = FloatWarperType::New();
                AllRibcage_GradInterpolator[j] = FloatInterpolatorType::New();
                
                RibcageGradientWarper[j]->SetInput( Orig_Grad_Reader[j]->GetOutput() );
                RibcageGradientWarper[j]->SetInterpolator( AllRibcage_GradInterpolator[j] );
                RibcageGradientWarper[j]->SetOutputSpacing( itkreader->GetOutput()->GetSpacing() );
                RibcageGradientWarper[j]->SetDisplacementField( RibcageField );
                RibcageGradientWarper[j]->SetOutputOrigin( itkreader->GetOutput()->GetOrigin() );
                RibcageGradientWarper[j]->Update();
                }

                cout << "Warp BodyMask with Ribcage Field" << endl;
                MaskWarperType::Pointer RibcageBodyWarper = MaskWarperType::New();
                MaskInterpolatorType::Pointer RibcageBody_Interpolator = MaskInterpolatorType::New();
                RibcageBodyWarper->SetInput(BodyMaskReader->GetOutput());
                RibcageBodyWarper->SetInterpolator(RibcageBody_Interpolator);
                RibcageBodyWarper->SetOutputSpacing( itkreader->GetOutput()->GetSpacing() );
                RibcageBodyWarper->SetDisplacementField( RibcageField );
                RibcageBodyWarper->SetOutputOrigin( itkreader->GetOutput()->GetOrigin() );
                RibcageBodyWarper->Update();

                cout << "The total field is calculated and used to warp the image" << endl;
                combinedMotionfield->SetRegions(itkreader->GetOutput()->GetLargestPossibleRegion());
                combinedMotionfield->SetSpacing(itkreader->GetOutput()->GetSpacing());
                combinedMotionfield->SetOrigin(itkreader->GetOutput()->GetOrigin());
                combinedMotionfield->Allocate();
                combinedMotionfield->FillBuffer(vectornomotion);
                
                //To save the correction to the Ribcage Motionfield
                VectorImageType::Pointer RibcageCorrection=VectorImageType::New();
                RibcageCorrection->SetRegions(itkreader->GetOutput()->GetLargestPossibleRegion());
                RibcageCorrection->SetSpacing(itkreader->GetOutput()->GetSpacing());
                RibcageCorrection->SetOrigin(itkreader->GetOutput()->GetOrigin());
                RibcageCorrection->Allocate();
                RibcageCorrection->FillBuffer(vectornomotion);
                
                //To save the correction to the lung motionfield
                VectorImageType::Pointer LungCorrection=VectorImageType::New();
                LungCorrection->SetRegions(itkreader->GetOutput()->GetLargestPossibleRegion());
                LungCorrection->SetSpacing(itkreader->GetOutput()->GetSpacing());
                LungCorrection->SetOrigin(itkreader->GetOutput()->GetOrigin());
                LungCorrection->Allocate();
                LungCorrection->FillBuffer(vectornomotion);

                //The motionfield orrection is added similar to Eiben et al., the fields are combined according to AddMap
                for (unsigned int x=0;x<outsize[0];++x)
                {
                for (unsigned int y=0;y<outsize[1];++y)
                    {
                    for (unsigned int z=0;z<outsize[2];++z)
                    {
                    VectorImageType::IndexType outindex;
                    outindex[0]=x;
                    outindex[1]=y;
                    outindex[2]=z;
                    
                    if (RibcageBodyWarper->GetOutput()->GetPixel(outindex)!=0)
                    { 
                    if (DiffusorMap->GetOutput()->GetPixel(outindex) != 0)
                    {	    
                        VectorImageType::PixelType RibcageCorrectVector = RibcageField->GetPixel(outindex);
                        VectorImageType::PixelType LungCorrectVector = LungField->GetPixel(outindex);
                        VectorImageType::PixelType CorrectionVectorLung;
                        VectorImageType::PixelType CorrectionVectorRibcage;
                        float RibcageGradLength_sq = RibcageGradientWarper[0]->GetOutput()->GetPixel(outindex)*RibcageGradientWarper[0]->GetOutput()->GetPixel(outindex) + RibcageGradientWarper[1]->GetOutput()->GetPixel(outindex)*RibcageGradientWarper[1]->GetOutput()->GetPixel(outindex) + RibcageGradientWarper[2]->GetOutput()->GetPixel(outindex)*RibcageGradientWarper[2]->GetOutput()->GetPixel(outindex);
                        float LungGradLength_sq = LungGradientWarper[0]->GetOutput()->GetPixel(outindex)*LungGradientWarper[0]->GetOutput()->GetPixel(outindex) + LungGradientWarper[1]->GetOutput()->GetPixel(outindex)*LungGradientWarper[1]->GetOutput()->GetPixel(outindex) + LungGradientWarper[2]->GetOutput()->GetPixel(outindex)*LungGradientWarper[2]->GetOutput()->GetPixel(outindex);
                                    
                        for (int j=0;j<3;++j)
                        {
                        float RibcageCorrTerm = (-1)*(SubMapRibcage->GetOutput()->GetPixel(outindex))*(RibcageGradientWarper[j]->GetOutput()->GetPixel(outindex))* (DiffusorMap->GetOutput()->GetPixel(outindex));
                        RibcageCorrTerm /=sqrt(RibcageGradLength_sq);
                        if (isnan(RibcageCorrTerm) || RibcageCorrTerm!=RibcageCorrTerm || !isfinite(RibcageCorrTerm)){RibcageCorrTerm = 0;}
                        RibcageCorrectVector[j] += RibcageCorrTerm;
                        CorrectionVectorRibcage[j] = RibcageCorrTerm;
                        float LungCorrTerm = (-1)*(SubMapLung->GetOutput()->GetPixel(outindex))*(LungGradientWarper[j]->GetOutput()->GetPixel(outindex))* (DiffusorMap->GetOutput()->GetPixel(outindex));
                        
                        LungCorrTerm /=sqrt(LungGradLength_sq);
                        if (isnan(LungCorrTerm) || (LungCorrTerm!=LungCorrTerm) || (!isfinite(LungCorrTerm))){LungCorrTerm = 0;}
                        CorrectionVectorLung[j] = LungCorrTerm;
                        LungCorrectVector[j] += LungCorrTerm;
                        }
                        RibcageCorrection->SetPixel(outindex, CorrectionVectorRibcage);
                        LungCorrection->SetPixel(outindex, CorrectionVectorLung);
                        RibcageField->SetPixel(outindex, RibcageCorrectVector);
                        
                        LungField->SetPixel(outindex, LungCorrectVector);
                    if (AddMap->GetOutput()->GetPixel(outindex)<=0) {combinedMotionfield->SetPixel(outindex, RibcageCorrectVector);}
                        else {combinedMotionfield->SetPixel(outindex, LungCorrectVector);}
                    }
                    else
                    {
                        if (AddMap->GetOutput()->GetPixel(outindex)<=0){ combinedMotionfield->SetPixel(outindex, RibcageField->GetPixel(outindex));}
                        else {combinedMotionfield->SetPixel(outindex, LungField->GetPixel(outindex));}
                    }
                    }
                    }
                    }
                }

                //The corrected and combined motionfield is saved
                string combined_outfilename;
                char buffer_combined[255];
                sprintf(buffer_combined,"%sMotionVectors/motionfield_%04d.mha",prefix.c_str(),step);
                combined_outfilename="";
                combined_outfilename.append(buffer_combined);
                cout << "Save combined motionfield at " << combined_outfilename << endl;
                VectorImageWriterType::Pointer combined_outwriter=VectorImageWriterType::New();
                combined_outwriter->SetInput(combinedMotionfield);
                combined_outwriter->SetUseCompression(true);     //Compressed Matlab files cannot be read by Matlab. combined_outfilename
                combined_outwriter->SetFileName(combined_outfilename);
                combined_outwriter->Update();
                combined_outwriter->Write();
                
                //The Ribcage and Lung Correction is interpolated
                RibcageCorrectionInterpolator->SetInputImage (RibcageCorrection);
                LungCorrectionInterpolator->SetInputImage (LungCorrection);
                
                
            } //end motionfield part
               
            else { //deformationfield (requires already calculated motionfield correction)
                                    
                //The deformationfields are corrected and comined as well
                cout << "Combine the deformationfields" << endl;
                VectorImageType::Pointer combinedDeformationfield = VectorImageType::New();
                combinedDeformationfield->SetRegions(itkreader->GetOutput()->GetLargestPossibleRegion());
                combinedDeformationfield->SetSpacing(itkreader->GetOutput()->GetSpacing());
                combinedDeformationfield->SetOrigin(itkreader->GetOutput()->GetOrigin());
                combinedDeformationfield->Allocate();
                combinedDeformationfield->FillBuffer(vectornomotion);
                
                ImageType::Pointer InterpolationValues = ImageType::New();
                InterpolationValues->SetRegions(itkreader->GetOutput()->GetLargestPossibleRegion());
                InterpolationValues->SetSpacing(itkreader->GetOutput()->GetSpacing());
                InterpolationValues->SetOrigin(itkreader->GetOutput()->GetOrigin());
                InterpolationValues->Allocate();
                InterpolationValues->FillBuffer(0);             
                
                /*
                the deformation field is corrected voxel by voxel. for each voxel the point where the original vector is pointing to is evaluated and the motionfield correction at this point is found with a
                linear interpolator( already implemented in itk). the motionfield correction vector is properly added (actually it is subtracted) and the new origin pixel of the deffield vector is found with rounding.
                the new deffield vector is stored in the new pixel. if there are already vectors in this pixel an unweighted average is used. afterwards the empty pixels are filled by averaging over the nearest
                neighbours which have a vector stored. if only two or less of the nearest neighbours are occupied the uncorrected vector is added to the nearest neighbour average.
                -> this proccess is quite messy and needs two iterations over all pixels. also it includes alot of rounding and interpolation. it might be good to find some better algortihm to really improve this :)
                */
                for (unsigned int x=0;x<outsize[0];++x)
                {
                for (unsigned int y=0;y<outsize[1];++y)
                    {
                    for (unsigned int z=0;z<outsize[2];++z)
                    {
                    VectorImageType::IndexType outindex;
                    outindex[0]=x;
                    outindex[1]=y;
                    outindex[2]=z;
                    if (BodyMaskReader->GetOutput()->GetPixel(outindex) != 0)
                    {
                        VectorImageType::PixelType NewVector2;
                        //See comment on Function DefCorrection
                        if (InternalMask->GetOutput()->GetPixel(outindex) != 0)
                        {
                            NewVector2 = DefCorrection(LungField, outindex, DiffusorMap->GetOutput(), LungCorrectionInterpolator);
                            
                        }
                        else 
                        {
                            NewVector2 = DefCorrection(RibcageField, outindex, DiffusorMap->GetOutput(), RibcageCorrectionInterpolator);
                            
                        }
                        VectorImageType::PixelType OldVector = combinedDeformationfield->GetPixel(outindex);		
                        //After the index is altered and the deformation vector corrected, the vector needs to be saved at the new index. However it might be that multiple deformation vectors are corrected
                        //to the same new index. Because of this an average has to be taken
                        int k =	InterpolationValues->GetPixel(outindex);		
                        for (unsigned int i=0;i<3;++i)
                        {
                            NewVector2[i] += k*OldVector[i];
                            NewVector2[i] /= (k+1);
                        }		
                        combinedDeformationfield->SetPixel(outindex,NewVector2);
                    
                        InterpolationValues->SetPixel(outindex, k+1);
                    }
                    }
                    }
                }
                //It is now possible, that for a voxel no deformation vector is assigned yet. Empty voxels are thus filled up with nearest neighbour average of the corrected deformationfield
                for (unsigned int x=0;x<outsize[0];++x)
                {
                for (unsigned int y=0;y<outsize[1];++y)
                    {
                    for (unsigned int z=0;z<outsize[2];++z)
                    {
                    VectorImageType::IndexType outindex;
                    outindex[0]=x;
                    outindex[1]=y;
                    outindex[2]=z;
                    if ((BodyMaskReader->GetOutput()->GetPixel(outindex) != 0) && (VectorNotZero(combinedDeformationfield->GetPixel(outindex))!=1))
                    {			
                        if (InternalMask->GetOutput()->GetPixel(outindex) != 0)
                        {
                            InterpolationValues->SetPixel(outindex, -1);
                            combinedDeformationfield->SetPixel(outindex,NearestNeighbourAverage(combinedDeformationfield,outindex, LungField->GetPixel(outindex),InterpolationValues));
                        }
                        else 
                        {
                            InterpolationValues->SetPixel(outindex, -1);
                            combinedDeformationfield->SetPixel(outindex,NearestNeighbourAverage(combinedDeformationfield,outindex, RibcageField->GetPixel(outindex),InterpolationValues));
                        }
                    }
                    }
                    }
                }
                
                //The corrected and combined deformationfield is saved
                char buffer_patched_deffield_combined[255];
                sprintf(buffer_patched_deffield_combined,"%sMotionVectors/deformationfield_%04d.mha",prefix.c_str(),step);
                string combinedPatched_deffield_outfilename="";
                combinedPatched_deffield_outfilename.append(buffer_patched_deffield_combined);
                cout << "Save combined deformationfield at " << combinedPatched_deffield_outfilename << endl;
                VectorImageWriterType::Pointer combinedPatchedDeffieldOutwriter=VectorImageWriterType::New();
                combinedPatchedDeffieldOutwriter->SetInput(combinedDeformationfield);
                combinedPatchedDeffieldOutwriter->SetUseCompression(true);     //Compressed Matlab files cannot be read by Matlab. combined_outfilename
                combinedPatchedDeffieldOutwriter->SetFileName(combinedPatched_deffield_outfilename);
                combinedPatchedDeffieldOutwriter->Update();
                combinedPatchedDeffieldOutwriter->Write();

                cout << "Writing warped timestep output with HU-intensity scaling" << endl;
                
                cout << "Creating a duplicate of the original CT to implement HU-intensity scaling" << endl;
                DuplicatorType::Pointer duplicator = DuplicatorType::New();
                duplicator->SetInputImage(itkreader->GetOutput());
                duplicator->Update();
                ImageType::Pointer densityinput = duplicator->GetOutput();
                cout << itkreader->GetOutput()->GetSpacing() << endl;
                cout << densityinput->GetSpacing() << endl;
                
                ImplementDensityChanges(densityinput, LungField, LungMaskReader->GetOutput());
                
                char buffer_warped[255];
                sprintf(buffer_warped, "%sMovingCT/state_%04d.mha",prefix.c_str(),step);
                string warpedfilename = "";
                warpedfilename.append(buffer_warped);
                cout << "Save Warped Image at " << warpedfilename << endl;
                //See comment at function WarpNWrite	
                WarpNWrite(densityinput, combinedMotionfield, warpedfilename);            
            }
        }
        
    }// end of each timestep
	cout << "-------------------------------------------------------------------------------";
	cout << "done !" << endl;
}

