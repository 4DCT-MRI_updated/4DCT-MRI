function individual_steps = GetParallelismDistribution(N_Cores, startstep, endstep)
    
    difference = endstep - startstep + 1;
    if (difference < N_Cores)
        N_Cores = difference;
    end

    intDiv = fix(difference / N_Cores);
    modDiv = mod(difference, N_Cores);
    individual_width = zeros(1,N_Cores) + intDiv;

    for i = 1:modDiv
        individual_width(i) = individual_width(i) + 1;
    end

    individual_steps = zeros(2,N_Cores);
    individual_steps(1,1) = startstep;
    individual_steps(2,1) = individual_steps(1,1) + individual_width(1) -1;
    for i= 2: N_Cores
        individual_steps(1,i) =  individual_steps(1,i-1) + individual_width(i-1);
        individual_steps(2,i) = individual_steps(1,i) + individual_width(i) -1 ;
    end
end