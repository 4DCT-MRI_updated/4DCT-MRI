function GetAndSaveDiffusor(Input_Filename, Output_Filename, DiffusionSteps)

    [MultMap header]=read_MHA(Input_Filename);
    DiffusedWeight = GetDiffusionMap(MultMap, header.spacing, DiffusionSteps);

    writemha(Output_Filename,DiffusedWeight,header.origin,header.spacing, 'float');
end


function res = GetDiffusionMap(MultiplyMap, dX, nTimeStepsIn)
    fixed = (MultiplyMap <= 0);

    diffCoeff = 0.35;
    dT = 0.5;
%     nTimeStepsIn = 5;

    res = zeros(size(fixed));
    res(fixed ~=0) = fixed(fixed~=0);
    for i = 1:nTimeStepsIn
        disp(i);
        res = res + diffCoeff*dT*(diffXX(res, dX(1)) + diffYY(res, dX(2)) + diffZZ(res, dX(3)));
        res(fixed ~=0) = fixed(fixed~=0);
    end
end

function output = diffXX(input, dX)
   output = zeros(size(input));
   output(2:end-1,:,:) = (1.0*input(1:end-2,:,:) - 2.0*input(2:end-1,:,:) + 1.0*input(3:end,:,:))/(2.0*dX);
   output( 1,:,:) = output( 2,:,:);
   output(end-1,:,:) = output(end-2,:,:);
end
function output = diffYY(input, dY)
   output = zeros(size(input));
   output(:,2:end-1,:) = (1.0*input(:,1:end-2,:) - 2.0*input(:,2:end-1,:) + 1.0*input(:,3:end,:))/(2.0*dY);
   output( :,1,:) = output(:,2,:);
   output(:,end-1,:) = output(:,end-2,:);
end
function output = diffZZ(input, dZ)
   output =  zeros(size(input));
   output(:,:,2:end-1) = (1.0*input(:,:,1:end-2) - 2.0*input(:,:,2:end-1) + 1.0*input(:,:,3:end))/(2.0*dZ);
   output(:,:,1) = output(:,:,2);
   output(:,:,end-1) = output(:,:,end-2);
end