clear;clc;close all;
addpath('../matlab-scripts/');
addpath('functions');

%Script to prepare plastimatch files to establish anatomical lung and ribcage correpsondence using deformable image registration (DIR)
%Created by Alisha Duetschler August 2019
%Adapted by Timothy Jenny August 2022

%Prequisites: 
%   -smooth lung and ribcage masks for CT and MRI (/structures/lungs_left_smooth.mha, 
%   /structures/lungs_right_smooth.mha and /structures/ribcage_smooth.mha)
%   -plastimatch installation

%   1) plastimatch B-spline registration between MRI and CT lung masks (for
%   each side separately) (DVF:MRI ---> CT )
%   2) In terminal navigate to code/2_meshCorrespondence folder and execute generated scripts (in
%   example/results/MRI<i>_CT<j> folder) using 'plastimatch register ../../example/results/MRI<i>_CT<j>/plastimatch_left.txt',  
%   'plastimatch register ../../example/results/MRI<i>_CT<j>/plastimatch_right.txt' and 'plastimatch register
%   ../../example/results/MRI<i>_CT<j>/plastimatch_ribcage.txt' (running
%   from a different location will require adaptation of paths)


%% input that has to be changed
CTNo = 1;
MRINo = 1;

CTbasepath = '../../example/data/CT/CT';
MRIbasepath = '../../example/data/MRI/MRI';
outputbasepath = '../../example/results/';


% enter landmark location if needed to guide registration
useLandmarksLeft = false;
useLandmarksRight = true;
useLandmarksRibcage = true;

% useLandmarks = true;
% if using landmarks to guide the registration save them in the follwing
% location und with the name lms_<side>_MRI.csv and lms_<side>_CT.csv
% fixedLandmarks = '../../example/results/MRI1_CT1/lms_right_MRI.csv';
% movingLandmarks = '../../example/results/MRI1_CT1/lms_right_CT.csv';
% and for ribcage:
% fixedLandmarks = '../../example/results/MRI1_CT1/lms_ribcage_MRI.csv';
% movingLandmarks = '../../example/results/MRI1_CT1/lms_ribcage_CT.csv';

%% Lungs
% perform for each side separately
outputFolder = [outputbasepath 'MRI' num2str(MRINo) '_CT' num2str(CTNo) '/'];

%% make output directory 
str=['mkdir -p ' outputFolder];
system(str);
    
for s = 1:2
    if s ==1
        str = 'left';
        useLandmarks = useLandmarksLeft;
    else
        str = 'right';
        useLandmarks = useLandmarksRight;
    end
    
    if useLandmarks == true
        fixedLandmarks = [outputbasepath 'MRI' num2str(MRINo) '_CT' num2str(CTNo) '/lms_' str '_MRI.csv'];
        movingLandmarks = [outputbasepath 'MRI' num2str(MRINo) '_CT' num2str(CTNo) '/lms_' str '_CT.csv'];
    else
        fixedLandmarks = ''
        movingLandmarks = ''
    end
        
    
    fixedMHA = [MRIbasepath num2str(MRINo) '/structures/lungs_' str '_smooth.mha'];
    movingMHA = [CTbasepath num2str(CTNo) '/structures/lungs_' str '_smooth.mha'];

    %% prepare files for plastimatch DIR     
    preparePlastimatchDIRfiles(fixedMHA,movingMHA,useLandmarks,fixedLandmarks,movingLandmarks,outputFolder,str)
    
end

%% Ribcage
str='ribcage';

if useLandmarksRibcage == true
    fixedLandmarks = [outputbasepath 'MRI' num2str(MRINo) '_CT' num2str(CTNo) '/lms_ribcage_MRI.csv'];
    movingLandmarks = [outputbasepath 'MRI' num2str(MRINo) '_CT' num2str(CTNo) '/lms_ribcage_CT.csv'];
else
    fixedLandmarks = ''
    movingLandmarks = ''
end
        
    
fixedMHA = [MRIbasepath num2str(MRINo) '/structures/ribcage_smooth.mha'];
movingMHA = [CTbasepath num2str(CTNo) '/structures/ribcage_smooth.mha'];
%% prepare files for plastimatch DIR 
preparePlastimatchDIRfiles(fixedMHA,movingMHA,useLandmarks,fixedLandmarks,movingLandmarks,outputFolder,str)



