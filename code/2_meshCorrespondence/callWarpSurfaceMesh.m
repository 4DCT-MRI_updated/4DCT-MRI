clear;clc;close all;
addpath('../matlab-scripts/');
addpath('functions');

%Deform MRI surface lung and ribcage meshes according to deformation vector fields from
%mask regirstration to establish anatomical correspondence
%Created by Alisha Duetschler August 2019
%Adapted by Timothy Jenny August 2022

%Prequisites: 
%   -CT and MRI lung and ribcage surface meshes (/structures/lungs_left.vkt, 
%   /structures/lungs_right.vtk and /structures/ribcage.vtk)
%   -results of image registration (vf_left.mha, vf_right.mha and vf_ribcage.mha in
%   example/results/MRI<i>_CT<j> folder)


%% input that has to be changed
CTNo = 1;
MRINo = 1;

CTbasepath = '../../example/data/CT/CT';
MRIbasepath = '../../example/data/MRI/MRI';
outputbasepath = '../../example/results/';

%% Lungs
% perform for each side separately

outputFolder = [outputbasepath 'MRI' num2str(MRINo) '_CT' num2str(CTNo) '/'];
for s = 1:2
    if s ==1
        str = 'left';
    else
        str = 'right';
    end   
        
    fixedSurfaceMesh = [MRIbasepath num2str(MRINo) '/structures/lungs_' str '.vtk'];
    movingSurfaceMesh = [CTbasepath num2str(CTNo) '/structures/lungs_' str '.vtk'];
    deformedSurfaceMesh = [outputFolder 'deformed_lungs_' str '.vtk'];

    %% copy lung meshes
    cmd = ['cp ' fixedSurfaceMesh ' ' outputFolder 'fixed_lungs_' str '.vtk'];
    system(cmd);
    cmd = ['cp ' movingSurfaceMesh ' ' outputFolder 'moving_lungs_' str '.vtk'];
    system(cmd);

    %% deform mesh
    disp(['deform surface mesh ' str]);
    dvf = [outputFolder 'vf_' str '.mha'];
    warpSurfaceMesh(fixedSurfaceMesh,dvf,deformedSurfaceMesh)
end


%% Ribcage
str = 'ribcage';
       
fixedSurfaceMesh = [MRIbasepath num2str(MRINo) '/structures/ribcage.vtk'];
movingSurfaceMesh = [CTbasepath num2str(CTNo) '/structures/ribcage.vtk'];
deformedSurfaceMesh = [outputFolder 'deformed_ribcage.vtk'];

%% copy lung meshes
cmd = ['cp ' fixedSurfaceMesh ' ' outputFolder 'fixed_ribcage.vtk'];
system(cmd);
cmd = ['cp ' movingSurfaceMesh ' ' outputFolder 'moving_ribcage.vtk'];
system(cmd);

%% deform mesh
disp(['deform surface mesh ribcage']);
dvf = [outputFolder 'vf_ribcage.mha'];
warpSurfaceMesh(fixedSurfaceMesh,dvf,deformedSurfaceMesh)



