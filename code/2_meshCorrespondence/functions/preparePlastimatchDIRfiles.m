function preparePlastimatchDIRfiles(fixedMHA,movingMHA,useLandmarks,fixedLandmarks,movingLandmarks,outputFolder,str)

dvf = [outputFolder 'vf_' str '.mha'];
warpedLandmarks = [outputFolder 'warped_lms_' str '.fcsv'];
warpedMHA = [outputFolder 'warped_mask_' str '.mha'];

plastimatchTemplate = fileread('plastimatch_align_center_affine_bspline_template.txt'); 
if useLandmarks == true  
    plastimatch =  ['[GLOBAL] \nfixed=' fixedMHA '\nmoving=' movingMHA '\n\nfixed_landmarks=' fixedLandmarks '\nmoving_landmarks=' movingLandmarks '\nwarped_landmarks=' warpedLandmarks '\n \nvf_out=' dvf '\nimg_out=' warpedMHA '\n\n' plastimatchTemplate];
else
    plastimatch =  ['[GLOBAL] \nfixed=' fixedMHA '\nmoving=' movingMHA '\n \nvf_out=' dvf '\nimg_out=' warpedMHA '\n\n' plastimatchTemplate];
end

plastimatch_File = [outputFolder 'plastimatch_' str '.txt'];        

fid=fopen(plastimatch_File,"w");
fprintf(fid, plastimatch);
fclose(fid);

end