function DIRmeshcorrespondence(fixedMHA,movingMHA,fixedSurfaceMesh,useLandmarks,fixedLandmarks,movingLandmarks,outputFolder,str)

%% plastimatch DIR
disp('------------------------------------------------------------------------------');
disp(['run deformable image registration ' str]);

preparePlastimatchDIRfiles(fixedMHA,movingMHA,useLandmarks,fixedLandmarks,movingLandmarks,outputFolder,str)

plastimatch_File = [outputFolder 'plastimatch_' str '.txt'];
runplastimatch=['plastimatch register ' plastimatch_File];
system(runplastimatch);


% %% calculate dice coefficients (original files and after DIR)
% disp('------------------------------------------------------------------------------');
% disp('calculate Dice coefficients and Hausdorff distances');
% dicestr=['echo BEFORE DIR > ' outputFolder 'dice_' str '.txt']; 
% system(dicestr);
% dicestr=['plastimatch dice --all ' fixedMHA ' ' movingMHA ' >> ' outputFolder 'dice_' str '.txt'];
% system(dicestr);
% dicestr=['echo ---------------------------- >> '  outputFolder 'dice_' str '.txt']; 
% system(dicestr);
% dicestr=['echo AFTER DIR >> ' outputFolder 'dice_' str '.txt']; 
% system(dicestr);
% dicestr=['plastimatch dice --all ' fixedMHA ' ' warpedMHA ' >> ' outputFolder 'dice_' str '.txt'];
% system(dicestr);

%% warp surface mesh
disp('------------------------------------------------------------------------------');
disp(['deform surface mesh ' str]);

if strcmp(str,'ribcage')
    deformedSurfaceMesh = [outputFolder 'deformed_' str '.vtk'];
else
    deformedSurfaceMesh = [outputFolder 'deformed_lungs_' str '.vtk'];
end
dvf = [outputFolder 'vf_' str '.mha'];
warpSurfaceMesh(fixedSurfaceMesh,dvf,deformedSurfaceMesh)







