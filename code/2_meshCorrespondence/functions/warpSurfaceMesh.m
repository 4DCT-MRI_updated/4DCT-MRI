function warpSurfaceMesh(fixedSurfaceMesh,dvf,deformedSurfaceMesh)

%% read input DVF
[DVF header]=read_Deformation(dvf,0);

%% Find image index of a point and apply DVF

[surfacepoints polys]=read_VTKPolyData(fixedSurfaceMesh,0,'float',0);
for j=1:length(surfacepoints)
    for i=1:3
        index(i)=round((surfacepoints(i,j)-header.origin(i))/header.spacing(i))+1;
    end
    surfacepoints(:,j)=surfacepoints(:,j)+DVF(:,index(1),index(2),index(3));
end
write_VTKPolyData(deformedSurfaceMesh,surfacepoints,polys,0);


