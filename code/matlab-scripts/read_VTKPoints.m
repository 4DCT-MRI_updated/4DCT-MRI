%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Reads VTKPolyData from ASCII file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [points] = read_VTKPoints(filename, verbose, numbertype)

if nargin < 2, verbose=0; end
if nargin < 3, numbertype = 'float'; end

if verbose
  fprintf('reading file "%s" ...\n',filename)
end

file = textread(filename,'%s','delimiter','\n','whitespace','');
len  = length(file);


[numPoints,count,errmsg,nextindex]=sscanf(file{5},['POINTS %d ' numbertype]); %*[. ]

if ~isempty(errmsg)
  error(['error while reading points, wrong type? ' errmsg])
end

% point list starts from line 6
[A,count,errmsg,nextindex]=sscanf(file{6},'%f %f %f %f %f %f %f %f %f');
lineindex=0;
if (count==9) 
    numLines = floor(numPoints/3);
    lineindex = numLines + 7;
elseif (count==3)
    numLines = numPoints;
    lineindex = numLines + 6;
end

for n = 1:numLines
  [A,count,errmsg,nextindex]=sscanf(file{n+5},'%f %f %f %f %f %f %f %f %f'); %*[. ]

  if ~isempty(errmsg)
    error(['error while reading points: ' errmsg])
  else
   if count==9  % this is equal to three points per line
    if exist('points')
      points = cat(2,points,[A(1:3),A(4:6),A(7:9)]);
    else
      points = [A(1:3),A(4:6),A(7:9)];
    end
   end
   if count==3
    if exist('points')
      points = cat(2,points,[A(1:3)]);
    else
      points = [A(1:3)];
    end
   end
  end
end

if (numLines<numPoints)
    if (mod(numPoints,3) == 1)
      [A,count,errmsg,nextindex]=sscanf(file{numLines+5+1},'%f %f %f'); %*[. ]

      if ~isempty(errmsg)
        error(['error while reading points: ' errmsg])
      else
        points = cat(2,points,A(1:3));
      end
    elseif (mod(numPoints,3) == 2)
      [A,count,errmsg,nextindex]=sscanf(file{numLines+5+1},'%f %f %f %f %f %f'); %*[. ]
      if ~isempty(errmsg)
        error(['error while reading points: ' errmsg])
      else
        points = cat(2,points,[A(1:3),A(4:6)]);
      end
    end
end
  
if verbose
  fprintf('   ... %d points read.\n',numPoints)
end