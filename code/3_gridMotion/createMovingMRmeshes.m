clear all; clc;close all;
addpath('../matlab-scripts');
addpath('functions');

%Script to create moving MR meshes
%Created by Alisha Duetschler June 2021
%Adapted by Timothy Jenny August 2022

% Prequisites: 
%   -MRI lung surface meshes and ribcage mesh
%   -MRI deformation vector fields

% Procedure:
%   1) Insert grid points inside MRI meshes (insertGridPointsInsideMRmesh.m)
%   2) Extract motion vectors at MRI mesh points and apply to mesh (readMotionvectorsforMesh.m)

%% 

%% input that has to be changed
MRINo = 1;

MRIbasepath = '../../example/data/MRI/MRI';
start_phase = 23; %one cycle
end_phase = 32;

%% define paths
MR_path = [MRIbasepath num2str(MRINo) '/']

MR_mesh_basepath = [MR_path '/structures/']
DVF_path=[MR_path 'dvfs/']; %MRI DVFs
MRI=[MR_path 'MRI.mha'];

%% set up directories
MovingMR_mesh_basepath = [MR_path '/MovingMRmesh/'];
cmd = ['mkdir -p ' MovingMR_mesh_basepath];
system(cmd);

%% for ribcage and both lung halves separately first insert points inside of the mesh and then apply the DVFs to the meshes
for s = 1:3
    if s == 1
        str = 'Left'
    elseif s == 2
        str = 'Right'
    else
        str = 'Ribcage'
    end
    
    %make directories for meshes
    cmd = ['mkdir -p ' MovingMR_mesh_basepath str];
    system(cmd);

    %%
    disp('---------------------------------------------------');
    disp('Insert grid points inside ' + str + ' meshes MR');
    % based on ray casting
    insertGridPointsInsideMRmesh(MR_mesh_basepath, MovingMR_mesh_basepath, str);  

    %%
    disp('---------------------------------------------------');
    disp('Extract motion vectors at MRI mesh points and apply to ' + str + ' mesh MR');

    for phase=start_phase:end_phase
        applyMotionToMRmesh(MovingMR_mesh_basepath, DVF_path, MRI, str, phase);
    end

    disp('---------------------------------------------------');

end
disp('---------------------------------------------------');



