function insertGridPointsInsideMRmesh(MR_mesh_basepath, MovingMR_mesh_basepath, str)

%% Script to insert a regular grid of points inside a reference surface lung mesh for a reference
% Written by Dirk Boye and Martin von Siebenthal
% Adapted to lung by Ye Zhang in August 2017
% Adapted by Alisha Duetschler in January 2020 and June 2021
% Adapted by Timothy Jenny in August 2022

    if strcmp(str,'Left') %left
        MR_mesh = [MR_mesh_basepath 'lungs_left.vtk'];
        outputbasename = [MovingMR_mesh_basepath 'lungs_left_'];
    elseif strcmp(str,'Right') %right    
        MR_mesh = [MR_mesh_basepath 'lungs_right.vtk'];
        outputbasename = [MovingMR_mesh_basepath 'lungs_right_'];
    else    
        MR_mesh = [MR_mesh_basepath 'ribcage.vtk'];
        outputbasename = [MovingMR_mesh_basepath 'ribcage_'];
    end
    disp('------------------------------------')
    disp(str)
    disp(MR_mesh)
    disp('-------------------------------------------------------')
    [refMRpoints,refMRpolys] = read_VTKPolyData(MR_mesh, 0, 'float',0); 

    allcoosOrigDef=refMRpoints;

    nPts   = size(allcoosOrigDef,2);
    len    = 3*nPts;

    % average shape
    meancorresp   = mean(allcoosOrigDef,2);
    meancorresp_x = meancorresp(1,:);
    meancorresp_y = meancorresp(2,:);
    meancorresp_z = meancorresp(3,:);

    allcoos=allcoosOrigDef-repmat(meancorresp,[1 nPts]); % normalized to mean position
    %%%%%%%%%%%%
    %% build regular grid

    % resolution of the regular grid in the mean mesh
    nXmm   = 15;
    nYmm   = 15;
    nZmm   = 15;

    minx = min(allcoos(1,:));
    maxx = max(allcoos(1,:));
    miny = min(allcoos(2,:));
    maxy = max(allcoos(2,:));
    minz = min(allcoos(3,:));
    maxz = max(allcoos(3,:));

    xvec = minx:nXmm:maxx;
    yvec = miny:nYmm:maxy;
    zvec = minz:nZmm:maxz;

    nGridPts = length(xvec)*length(yvec)*length(zvec);

    [X,Y,Z] = meshgrid(xvec,yvec,zvec);
    gridX = reshape(X,1,nGridPts);
    gridY = reshape(Y,1,nGridPts);
    gridZ = reshape(Z,1,nGridPts);

    location_x = allcoos(1,:);
    location_y = allcoos(2,:);
    location_z = allcoos(3,:);

    % get defined points for convex hull
    val_x      = allcoos(1,:);
    ival_x_all = griddata(location_x,location_y,location_z,val_x,gridX,gridY,gridZ);
    defPts = find(~isnan(ival_x_all));
    nDefPts = length(defPts);

    disp(sprintf('%d points of %d within convex hull',nDefPts,nGridPts))

    %% brute force test if point is inside or outside the non-convex mean surface

    nPolys = length(refMRpolys); %%
    P0 = [0 0 0];
    small_num = 0.0001;
    defInsidePts = [];

    for p = 1:nDefPts

        pi = defPts(p);            
        P1 = [gridX(pi),gridY(pi),gridZ(pi)];
        inside = PointInsideVolume(P1, refMRpolys', allcoos'); %%

        if inside
            defInsidePts = [defInsidePts pi];
            disp(sprintf('no intersection found for point %d',pi))
        end
    end % for each point

    %%
    disp(sprintf('%d of %d defined points are inside the non-convex surface',length(defInsidePts),length(defPts)))

    newdefPts = defInsidePts;
    nNewDefPts = length(newdefPts);
    undefPts = setdiff([1:nGridPts],newdefPts);

    val_x_surface = allcoos(1,:);
    val_y_surface = allcoos(2,:);
    val_z_surface = allcoos(3,:);

    val_x_grid = gridX(newdefPts);
    val_y_grid = gridY(newdefPts);
    val_z_grid = gridZ(newdefPts);

    length(val_x_surface);
    length(val_x_grid);

    % append the surface points
    ival_x_surface_grid = [val_x_surface val_x_grid];
    ival_y_surface_grid = [val_y_surface val_y_grid];
    ival_z_surface_grid = [val_z_surface val_z_grid];

    MRlung_surface_grid=[ival_x_surface_grid+meancorresp_x; ival_y_surface_grid+meancorresp_y; ival_z_surface_grid+meancorresp_z ];
    MRlung_grid=[val_x_grid+meancorresp_x; val_y_grid+meancorresp_y; val_z_grid+meancorresp_z ];
    MRlung_surface=[val_x_surface+meancorresp_x; val_y_surface+meancorresp_y; val_z_surface+meancorresp_z ];
    
    write_VTKPolyData([outputbasename 'surface.vtk'],MRlung_surface,refMRpolys);
    write_VTKPolyData([outputbasename 'surface_and_grid.vtk'],MRlung_surface_grid,refMRpolys);
    write_VTKPoints([outputbasename 'grid.vtk'],MRlung_grid);

end

