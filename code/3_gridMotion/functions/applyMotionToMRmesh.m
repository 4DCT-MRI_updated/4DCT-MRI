function applyMotionToMRmesh(MRbasepathoutput, DVF_path, MRI, Side, phase)

if strcmp(Side,'Left') %left
    reffileMR=[MRbasepathoutput '/lungs_left_surface_and_grid.vtk'];
    deffield=[DVF_path 'internal_state_' num2str(phase,'%04d') '.mha'];
elseif strcmp(Side,'Right') %right  
    reffileMR=[MRbasepathoutput '/lungs_right_surface_and_grid.vtk'];
    deffield=[DVF_path 'internal_state_' num2str(phase,'%04d') '.mha'];
else %strcmp(Side,'Right') %right  
    reffileMR=[MRbasepathoutput '/ribcage_surface_and_grid.vtk'];
    deffield=[DVF_path 'external_state_' num2str(phase,'%04d') '.mha'];
end

outMR=[MRbasepathoutput Side];

outputfileMR=[outMR '/state_' num2str(phase,'%04d') '.vtk'];

[motion header]=read_Deformation(deffield,0);

[mha headermha]=read_MHA(MRI);

[refpointsMR polys]=read_VTKPolyData(reffileMR,0,'float',0); 
%% Find image index of a point


for j=1:length(refpointsMR)
    for i=1:3
        index(i)=round((refpointsMR(i,j)-headermha.origin(i))/headermha.spacing(i))+1;

    end
    newpointsMR(:,j)=refpointsMR(:,j)+motion(:,index(1),index(2),index(3));
    
end
%%


disp(['state' num2str(phase) ' done'])
write_VTKPolyData(outputfileMR,newpointsMR,polys,0);
close