function insertGridPointsInsideCTmesh(CT_mesh_basename, MovingMR_mesh_basepath, outputbasepath, str)

%% Script to insert a regular grid of points inside a lung mesh for a reference, and then adds the corresponding points to a another mesh
% Written by Dirk Boye and Martin von Siebenthal
% Adapted to lung by Ye Zhang in August 2017
% Adapted by Alisha Duetschler in January 2020 and June 2021
% Adapted by Timothy Jenny in August 2022

    if strcmp(str,'Left') %left
        CT_mesh = [CT_mesh_basename 'lungs_left.vtk'];
        MR_surface_mesh = [MovingMR_mesh_basepath 'lungs_left_surface.vtk']
        MR_grid = [MovingMR_mesh_basepath 'lungs_left_grid.vtk']
        outputbasename = [outputbasepath 'lungs_left_'];
    elseif strcmp(str,'Right') %right    
        CT_mesh = [CT_mesh_basename 'lungs_right.vtk']
        MR_surface_mesh = [MovingMR_mesh_basepath 'lungs_right_surface.vtk'];
        MR_grid = [MovingMR_mesh_basepath 'lungs_right_grid.vtk'];
        outputbasename = [outputbasepath 'lungs_right_']; 
    elseif strcmp(str,'Ribcage') %right    
        CT_mesh = [CT_mesh_basename 'ribcage.vtk']
        MR_surface_mesh = [MovingMR_mesh_basepath 'ribcage_surface.vtk'];
        MR_grid = [MovingMR_mesh_basepath 'ribcage_grid.vtk'];
        outputbasename = [outputbasepath 'ribcage_']; 
    end
    
    disp('-------------------------------')
    disp(CT_mesh)
    disp('-------------------------------')
    [refCTpoints,refCTpolys] = read_VTKPolyData(CT_mesh, 0, 'float',0);
    [refMRpoints,refMRpolys] = read_VTKPolyData(MR_surface_mesh, 0, 'float',0); 
    
    [MRgridpoints] = read_VTKPoints(MR_grid,  0, 'float');

    allcoosOrigDef=refMRpoints;
    allcoosOrigDef2=refCTpoints;
    
    assert(isequal(size(refCTpoints),size(refMRpoints)),'Number of surface grid points of MR and CT lung meshes are not the same!');

    nPts   = size(allcoosOrigDef,2);
    nGridPts = size(MRgridpoints,2);
    len    = 3*nPts;

    % average shape
    meancorresp   = mean(allcoosOrigDef,2);
    meancorresp_x = meancorresp(1,:);
    meancorresp_y = meancorresp(2,:);
    meancorresp_z = meancorresp(3,:);

    allcoos=allcoosOrigDef-repmat(meancorresp,[1 nPts]); % normalized to mean position
    shiftedMRgrid = MRgridpoints-repmat(meancorresp,[1 nGridPts]);

    %%%%%%%%%%%%

    val_x_grid = allcoos(1,:);
    val_y_grid = allcoos(2,:);
    val_z_grid = allcoos(3,:);
    
    val_x_surface = shiftedMRgrid(1,:);
    val_y_surface = shiftedMRgrid(2,:);
    val_z_surface = shiftedMRgrid(3,:);
    
    ival_x_surface_grid = [val_x_surface val_x_grid];
    ival_y_surface_grid = [val_y_surface val_y_grid];
    ival_z_surface_grid = [val_z_surface val_z_grid];
    
    gridX = reshape(val_x_surface,1,nGridPts);
    gridY = reshape(val_y_surface,1,nGridPts);
    gridZ = reshape(val_z_surface,1,nGridPts);

    location_x = allcoos(1,:);
    location_y = allcoos(2,:);
    location_z = allcoos(3,:);
    
    
    
    %% new object

    % average shape
    meancorresp_new   = mean(allcoosOrigDef2,2);
    meancorresp_x_new = meancorresp_new(1,:);
    meancorresp_y_new = meancorresp_new(2,:);
    meancorresp_z_new = meancorresp_new(3,:);

    disp(size(allcoosOrigDef2))
    disp(size(allcoosOrigDef))
    disp(nPts)


    allcoos_new=allcoosOrigDef2-repmat(meancorresp_new,[1 nPts]); % normalized to mean position

    val_x2_surface  = allcoos_new(1,:);
    val_y2_surface  = allcoos_new(2,:);
    val_z2_surface  = allcoos_new(3,:);

    % the points (val_x,val_y,val_z) that correspond to the mean shape contour points
    % (location_x,location_y,location_z) are known. linearly interpolate from
    % this data to get the points that correspond to the regular grid points
    % (gridX,gridY,gridZ).
    val_x2_grid = griddata(location_x,location_y,location_z,val_x2_surface,gridX,gridY,gridZ);
    val_y2_grid = griddata(location_x,location_y,location_z,val_y2_surface,gridX,gridY,gridZ);
    val_z2_grid = griddata(location_x,location_y,location_z,val_z2_surface,gridX,gridY,gridZ);

    defPts2 = find(~isnan(val_x2_grid));
    nDefPts2 = length(defPts2)
    undefPts2 = find(isnan(val_x2_grid));

    disp(sprintf('%d of %d points defined for new subject.',nDefPts2,nGridPts))

    % append the surface points
    ival_x2_surface_grid = [val_x2_surface val_x2_grid];
    ival_y2_surface_grid = [val_y2_surface val_y2_grid];
    ival_z2_surface_grid = [val_z2_surface val_z2_grid];


    CTlung_surface_grid=[ival_x2_surface_grid+meancorresp_x_new; ival_y2_surface_grid+meancorresp_y_new; ival_z2_surface_grid+meancorresp_z_new ];
    CTlung_grid=[val_x2_grid+meancorresp_x_new; val_y2_grid+meancorresp_y_new; val_z2_grid+meancorresp_z_new ];

    write_VTKPolyData([outputbasename 'surface_and_grid.vtk'],CTlung_surface_grid,refCTpolys);
    write_VTKPoints([outputbasename 'grid.vtk'],CTlung_grid);

end

