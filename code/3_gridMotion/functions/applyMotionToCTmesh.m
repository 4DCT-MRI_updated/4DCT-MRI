function applyMotionToCTmesh(MovingMR_mesh_basepath, MovingCT_mesh_basepath, str, phase)

if strcmp(str,'Left') %left
    refFileCT=[MovingCT_mesh_basepath '/lungs_left_surface_and_grid.vtk'];
    refFileMR=[MovingMR_mesh_basepath '/lungs_left_surface_and_grid.vtk'];
elseif strcmp(str,'Right') %right  
    refFileCT=[MovingCT_mesh_basepath '/lungs_right_surface_and_grid.vtk'];
    refFileMR=[MovingMR_mesh_basepath '/lungs_right_surface_and_grid.vtk'];
elseif strcmp(str,'Ribcage') %right  
    refFileCT=[MovingCT_mesh_basepath '/ribcage_surface_and_grid.vtk'];
    refFileMR=[MovingMR_mesh_basepath '/ribcage_surface_and_grid.vtk'];
end

movingFileMR = [MovingMR_mesh_basepath str '/state_' num2str(phase,'%04d') '.vtk'];
outputFileCT=[MovingCT_mesh_basepath str '/state_' num2str(phase,'%04d') '.vtk'];

[refPointsMR polys]=read_VTKPolyData(refFileMR,0,'float',0); 
[refPointsCT polys]=read_VTKPolyData(refFileCT,0,'float',0); 

[movingPointsMR polys]=read_VTKPolyData(movingFileMR,0,'float',0); 

%% Find image index of a point

for j=1:length(refPointsMR)
    for i=1:3
%         disp(movingFileMR)
%         test1 = movingPointsMR(i,j);
%         test2 = refPointsMR(i,j);
        motion = movingPointsMR(i,j)-refPointsMR(i,j);
        newpointsCT(i,j)=refPointsCT(i,j)+motion;
    end
end
%%
disp(['state' num2str(phase) ' done'])
disp(outputFileCT)
write_VTKPolyData(outputFileCT,newpointsCT,polys,0);
close