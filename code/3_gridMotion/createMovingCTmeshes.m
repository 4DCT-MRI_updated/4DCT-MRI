clear all; clc; close all;
addpath('../matlab-scripts');
addpath('functions');

%Script to create moving CT meshes
%Created by Alisha Duetschler August 2019
%Adapted in June 2021
%Adapted by Timothy Jenny in August 2022 to further include ribcage motion

%Prequisites: 
%   -Corresponding mesh of surface of lungs and ribcage for CT and MRI
%   -Moving MR lung and ribcage meshes

%   1) Insert corresponding grid points inside CT meshes (insertGridPointsInsideCTmesh.m)
%   2) Extract motion vectors from moving MRI mesh points and apply to CT mesh
%      (applyMotionToCTmesh.m)

%% 

%% input that has to be changed
CTNo = 1;
MRINo = 1;

CTbasepath = '../../example/data/CT/CT';
MRIbasepath = '../../example/data/MRI/MRI';
CT_MR_basepath = '../../example/results/';

start_phase = 23; %one cycle
% end_phase = 32;

%% define paths
MR_path = [MRIbasepath num2str(MRINo) '/']

CT_MR_path = [CT_MR_basepath 'MRI' num2str(MRINo) '_CT' num2str(CTNo) '/'];

CT_mesh_basename = [CT_MR_path 'deformed_'];
MovingMR_mesh_path = [MR_path 'MovingMRmesh/'];

%% set up directories
cmd = ['mkdir -p ' CT_MR_path];
system(cmd);

MovingCT_mesh_basepath = [CT_MR_path '/MovingCTmesh/'];
cmd=['mkdir -p ' MovingCT_mesh_basepath];
system(cmd);

%% for ribcage and both lung halves separately first insert points inside of the mesh and then apply the DVFs to the meshes

for s = 1:3
    if s == 1
        str = 'Left'
    elseif s == 2
        str = 'Right'
    else
        str = 'Ribcage'
    end
   
   %make directories for moving meshes    
   cmd=['mkdir -p ' MovingCT_mesh_basepath str '/'];
   system(cmd);   

   %%
   disp('---------------------------------------------------');
   disp(['Insert grid points inside mesh ' str ' CT']);
   % based on ray casting
   insertGridPointsInsideCTmesh(CT_mesh_basename, MovingMR_mesh_path, MovingCT_mesh_basepath, str);
   
   %%
   disp('---------------------------------------------------');
   disp(['Extract motion vectors at MRI mesh points and apply to mesh ' str ' CT']);

   for phase=start_phase:end_phase
       applyMotionToCTmesh(MovingMR_mesh_path, MovingCT_mesh_basepath, str, phase);
   end
   disp('---------------------------------------------------');

end

disp('---------------------------------------------------');


