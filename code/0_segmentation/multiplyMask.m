addpath('../matlab-scripts');

input_0_1_file = '...';
output_0_255_file = '...';

%%
[image_data image_info] = read_MHA(file, true);

dims = size(image_data);

new_image_data = 255* image_data;

writemha(output_0_255_file,new_image_data,image_info.origin,image_info.spacing,'uchar');

