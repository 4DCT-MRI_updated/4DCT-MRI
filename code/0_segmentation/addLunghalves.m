addpath('../matlab-scripts');

inputPathMask1 = '../../example/data/CT/CT1/structures/lungs_left.mha';
inputPathMask2 = '../../example/data/CT/CT1/structures/lungs_right.mha';
outputPathMask = '../../example/data/CT/CT1/structures/lungs.mha';

%%
[mask1 header1 ] = read_MHA(inputPathMask1, true);
[mask2 header2 ] = read_MHA(inputPathMask2, true);


assert(isequal(header1,header2), "both masks should have the same header");

combinedMask = ((mask1 + mask2) > 0)*255;
writemha(outputPathMask,combinedMask,header1.origin,header1.spacing,'uchar');
