clear all; clc; close all;
addpath('../matlab-scripts');
% Creates internal region mask based on body and ribcage mask
%% input that has to be changed
CTorMRINo = 1;

CTorMRIbasepath = '../../example/data/MRI/MRI'; 
% CTorMRIbasepath = '../../example/data/CT/CT';

%% define paths
CTorMRI_path = [CTorMRIbasepath num2str(CTorMRINo) '/']

ribcage_mask=[CTorMRI_path 'structures/ribcage.mha'];
body_mask = [CTorMRI_path 'structures/body.mha'];

internal_mask= [CTorMRI_path 'structures/internal_region.mha'];

%% Create internal region masks
[body header]=read_MHA(body_mask); 
[ribcage header]=read_MHA(ribcage_mask); 

internal = 255* ((body - ribcage)>0);
writemha(internal_mask , internal , header.origin , header.spacing,'uchar');
